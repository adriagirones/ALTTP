package Enemies;

import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;
import Modules.ModuleEnemies.ENEMY_MOVE;
import Textures.Terrain;
import Textures.Texture;

public class SwordSoldier extends Enemy {
	
	int original_posX;
	int original_posY;
	float speed = (float) (1 * Main.size);
	boolean activePath = false;
	int cont = 0;
	
	Texture swordsoldier = null;
	Animation walkingRight = new Animation();
	Animation walkingLeft = new Animation();
	Animation walkingUp = new Animation();
	Animation walkingDown = new Animation();
	
	public SwordSoldier(int x, int y, ENEMY_MOVE move, int live, TypeDrops drop, int droprate) {
		super(x, y, move, live, drop, droprate);
		this.original_posX = x;
		this.original_posY = y;
		
		this.texture = swordsoldier = new Texture("swordsoldier", original_posX, original_posY, original_posX+18, original_posY+28, "");
		swordsoldier.enemy = true;
		
		walkingRight.PushBack("assets/enemies/swordsoldierR.png",18,28);
		walkingLeft.PushBack("assets/enemies/swordsoldierL.png",18,28);
		walkingUp.PushBack("assets/enemies/swordsoldierU.png",18,28);
		walkingDown.PushBack("assets/enemies/swordsoldier.png",18,28);
		
		this.animationenemy = walkingDown;
	}
	
	public void Move() {
		switch(this.move) {
		case SWORD_SOLDIER_1:
			Move_Sword_Soldier_1();
			break;
		case SWORD_SOLDIER_2:
			Move_Sword_Soldier_2();
		default:
			break;
		}
		
	}
	
	public void Collision() {
		for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
			if((Main.App.textures.sprites.get(i) instanceof Terrain || Main.App.textures.sprites.get(i).enemy == true 
					|| (Main.App.textures.sprites.get(i).pj && Main.App.player.live == 0)) && swordsoldier.leftOn(Main.App.textures.sprites.get(i))) {
				swordsoldier.x1+=speed;
				swordsoldier.x2+=speed;
			}
			if((Main.App.textures.sprites.get(i) instanceof Terrain || Main.App.textures.sprites.get(i).enemy == true
					|| (Main.App.textures.sprites.get(i).pj && Main.App.player.live == 0)) && swordsoldier.rightOn(Main.App.textures.sprites.get(i))) {
				swordsoldier.x1-=speed;
				swordsoldier.x2-=speed;

			}
			if((Main.App.textures.sprites.get(i) instanceof Terrain || Main.App.textures.sprites.get(i).enemy == true
					|| (Main.App.textures.sprites.get(i).pj && Main.App.player.live == 0 )) && swordsoldier.headOn(Main.App.textures.sprites.get(i))) {
				swordsoldier.y1+=speed;
				swordsoldier.y2+=speed;
			}
			if((Main.App.textures.sprites.get(i) instanceof Terrain || Main.App.textures.sprites.get(i).enemy == true
					|| (Main.App.textures.sprites.get(i).pj && Main.App.player.live == 0)) && swordsoldier.stepsOn(Main.App.textures.sprites.get(i))) {
				swordsoldier.y1-=speed;
				swordsoldier.y2-=speed;
			}
		}
		
	}
	
	private void Move_Sword_Soldier_1() {
		if(flagMove) {
			cont++;
			if(cont < 60) {
				this.animationenemy = walkingRight;
				swordsoldier.x1 += speed;
				swordsoldier.x2 += speed;
			}
			else {
				this.animationenemy = walkingLeft;
				swordsoldier.x1 -= speed;
				swordsoldier.x2 -= speed;
				if(cont == 120) {
					cont = 0;
				}
			}
		}
		
	}

	private void Move_Sword_Soldier_2() {
		int diffX = Main.App.player.link.x1 - swordsoldier.x1;
		int diffY = Main.App.player.link.y1 - swordsoldier.y1;
		
		if(diffX<150 && diffY<150 && diffX>-150 && diffY>-150) {
			activePath = true;
		}
		
		if(activePath && flagMove) {
			float angle = (float)Math.atan2(diffY, diffX);
			
			int cosX = 0;
			int cosY = 0;
			
			//Normalize the angle so that it always has the same speed
			if(Math.cos(angle) > 0) {
				cosX = (int)speed;
			}
			else {
				cosX = (int)-speed;
			}
			
			if(Math.sin(angle) > 0) {
				cosY = (int)speed;
			}
			else {
				cosY = (int)-speed;
			}
			
			//Move the enemy on one axis only if the character is right on the same axis.
			if(Math.sin(angle) < 0.98 && Math.sin(angle) > -0.98) {
				swordsoldier.x1 += cosX;
				swordsoldier.x2 += cosX;
				if(cosX>0) {
					this.animationenemy = walkingRight;
				}
				else {
					this.animationenemy = walkingLeft;
				}	
			}
			if(Math.cos(angle) < 0.98 && Math.cos(angle) > -0.98) {
				swordsoldier.y1 += cosY;
				swordsoldier.y2 += cosY;
				if(cosY>0) {
					this.animationenemy = walkingDown;
				}	
				else {
					this.animationenemy = walkingUp;
				}
					
			}
		}
	}

	@Override
	public boolean Death() {
		textdeath.x1 = swordsoldier.x1;
		textdeath.y1 = swordsoldier.y1;
		textdeath.changeFrame(deathAnimation.GetCurrentFrame(), deathAnimation.newWidth(), deathAnimation.newHeight());
		Main.App.textures.sprites.add(textdeath);
		
		if(deathAnimation.Finished()) {
			Main.App.entities.addDrop(swordsoldier.x1, swordsoldier.y1, drop, droprate);
			return true;
		}
		else {
			return false;
		}
		
	}
	
	

}
