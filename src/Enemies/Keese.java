package Enemies;

import java.util.Random;

import Enums.Direction;
import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;
import Modules.ModuleEnemies.ENEMY_MOVE;
import Textures.Texture;

public class Keese extends Enemy {
	
	int original_posX;
	int original_posY;
	float speed = (float) (1.5 * Main.size);
	int cont = 0;
	int min_x, max_x, min_y, max_y;
	boolean activePath = false;
	
	Texture keese = null;
	Animation idle = new Animation();
	Animation flying = new Animation();

	
	public Keese(int x, int y, ENEMY_MOVE move, int live, TypeDrops drop, int droprate) {
		super(x, y, move, live, drop, droprate);
		this.original_posX = x;
		this.original_posY = y;
		
		this.texture = keese = new Texture("keese", original_posX, original_posY, original_posX+12, original_posY+16, "");
		keese.enemy = true;
		
		flying.PushBack("assets/enemies/keeseflying1.png",16,13);
		flying.PushBack("assets/enemies/keeseflying2.png",16,13);
		flying.speed = 0.05f;
		
		idle.PushBack("assets/enemies/keeseidle.png",12,14);
		
		min_x = (original_posX*Main.size)-(100*Main.size);
		max_x = (original_posX*Main.size)+(100*Main.size);
		min_y = (original_posY*Main.size)-(100*Main.size);
		max_y = (original_posY*Main.size)+(100*Main.size);
		
		this.animationenemy = idle;
	}

	public void Move() {
		switch(this.move) {
		case KEESE_V:
			Move_Keese_Verical();
			break;
		case KEESE_H:
			Move_Keese_Horizontal();
			break;
		default:
			break;
		}
		
	}
	
	private void Move_Keese_Horizontal() {
		int diffX = Main.App.player.link.x1 - keese.x1;
		int diffY = Main.App.player.link.y1 - keese.y1;
		
		if(diffX<50 && diffY<50 && diffX>-50 && diffY>-50) {
			activePath = true;
		}
		else {
			activePath=false;
			this.animationenemy = idle;
		}
		if(flagMove && activePath) {
			cont++;
			this.animationenemy = flying;
			if(cont < 50) {
				keese.x1 += speed;
				keese.x2 += speed;
			}
			else {
				keese.x1 -= speed;
				keese.x2 -= speed;
				if(cont == 100) {
					cont = 0;
				}
			}
		}
	}

	private void Move_Keese_Verical() {
		int diffX = Main.App.player.link.x1 - keese.x1;
		int diffY = Main.App.player.link.y1 - keese.y1;
		
		if(diffX<150 && diffY<150 && diffX>-150 && diffY>-150) {
			activePath = true;
		}
		else {
			activePath=false;
			this.animationenemy = idle;
		}
		if(flagMove && activePath) {
			cont++;
			this.animationenemy = flying;
			if(cont < 60) {
				keese.y1 += speed;
				keese.y2 += speed;
				if(cont >= 0 && cont < 10 || cont >= 20 && cont < 30 || cont >= 40 && cont < 50) {
					keese.x1 += speed*2;
					keese.x2 += speed*2;
				}
				else {
					keese.x1 -= speed*2;
					keese.x2 -= speed*2;
				}
		
			}
			else {
				keese.y1 -= speed;
				keese.y2 -= speed;
				if(cont >= 60 && cont < 70 || cont >= 80 && cont < 90 || cont >= 100 && cont < 110) {
					keese.x1 += speed*2;
					keese.x2 += speed*2;
				}
				else {
					keese.x1 -= speed*2;
					keese.x2 -= speed*2;
				}
				if(cont == 120) {
					cont = 0;
				}
			}
			if(cont%25==0 || cont==1) {
				Main.App.window.w.playSFX("assets/sfx/keese.wav");
			}
		}
	}

	public void Collision() {
		if(activePath) {
			for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
				if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true) && keese.leftOn(Main.App.textures.sprites.get(i))) {
					keese.x1+=speed;
					keese.x2+=speed;
				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						 && keese.rightOn(Main.App.textures.sprites.get(i))) {
					keese.x1-=speed;
					keese.x2-=speed;

				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						 && keese.headOn(Main.App.textures.sprites.get(i))) {
					keese.y1+=speed;
					keese.y2+=speed;
				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						&& keese.stepsOn(Main.App.textures.sprites.get(i))) {
					keese.y1-=speed;
					keese.y2-=speed;
				}
			}
		}
	}
	
	@Override
	public boolean Death() {
		textdeath.x1 = keese.x1;
		textdeath.y1 = keese.y1;
		textdeath.changeFrame(deathAnimation.GetCurrentFrame(), deathAnimation.newWidth(), deathAnimation.newHeight());
		Main.App.textures.sprites.add(textdeath);
		
		if(deathAnimation.Finished()) {
			Main.App.entities.addDrop(keese.x1, keese.y1, drop, droprate);
			return true;
		}
		else {
			return false;
		}
		
	}

}
