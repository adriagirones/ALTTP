package Enemies;

import Enums.TypeDrops;
import Modules.Animation;
import Modules.ModuleEnemies.ENEMY_MOVE;
import Modules.ModuleEnemies.ENEMY_TYPES;
import Textures.Texture;

/**
 * Abstract class of a Enemy
 * @author Adria Girones Calzada
 *
 */
public abstract class Enemy {
	
	/**
	 * Type of the enemy.
	 */
	public ENEMY_TYPES type;
	
	/**
	 * Movement of the enemy.
	 */
	public ENEMY_MOVE move;
	
	/**
	 * Identifier of the enemy.
	 */
	public int id;
	
	/**
	 * Horizontal position of the enemy.
	 */
	int x;
	
	/**
	 * Vertical position of the enemy.
	 */
	int y;
	
	/*
	 * Live of the enemy.
	 */
	public int live = 0;
	
	/**
	 * Texture of the enemy.
	 */
	public Texture texture = null;
	
	/**
	 * Animation of the enemy.
	 */
	public Animation animationenemy = new Animation();
	
	/**
	 * Animation of the enemy when it dies
	 */
	public Animation deathAnimation = new Animation();
	
	/**
	 * Texture of the death animation
	 */
	Texture textdeath = null;
	
	/**
	 * Flag of the move of the enemy. If it is true, the enemy moves
	 */
	public boolean flagMove = true;
	
	/**
	 * Type of the drop of the enemy.
	 */
	TypeDrops drop = TypeDrops.NONE;
	
	/**
	 * Drop rate (It must be a integer from 0 to 10)
	 */
	int droprate;
	
	/**
	 * Constructor for an enemy.
	 * @param x Horizontal position where the enemy will be created.
	 * @param y Vertical position where the enemy will be created.
	 * @param move  Which move will it has.
	 * @param live Live of the enemy.
	 * @param drop Type of the drop.
	 * @param droprate Drop rate.
	 */
	public Enemy(int x, int y, ENEMY_MOVE move, int live, TypeDrops drop, int droprate) {
		this.x = x;
		this.y = y;
		this.move = move;
		this.live = live;
		deathAnimation.PushBack("assets/enemies/deathanimation1.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation2.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation3.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation4.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation5.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation6.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation7.png",24,22);
		deathAnimation.PushBack("assets/enemies/deathanimation8.png",24,22);
		deathAnimation.loop=false;
		deathAnimation.speed=0.15f;
		textdeath = new Texture("swordsoldier", this.x,  this.y, this.x+10, this.y+10, "");
		this.drop=drop;
		this.droprate=droprate;
	}
	
	/**
	 * Does the movement of each character.
	 */
	public abstract void Move();
	
	/**
	 * Look at each enemy's collisions with his environment.
	 */
	public abstract void Collision();
	
	/**
	 * Do the deathAnimation
	 * @return true if the deathAnimation has finished<br>
	 * false if the deathAnimation has not finished
	 */
	public abstract boolean Death();
	

}
