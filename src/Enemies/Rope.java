package Enemies;

import java.util.Random;

import Enums.Direction;
import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;
import Modules.ModuleEnemies.ENEMY_MOVE;
import Textures.Texture;

public class Rope extends Enemy {
	
	int original_posX;
	int original_posY;
	float speed = (float) (1 * Main.size);
	int cont = 0;
	int min_x, max_x, min_y, max_y;
	boolean activePath = false;
	
	Texture rope = null;
	Animation walkingRight = new Animation();
	Animation walkingLeft = new Animation();
	Animation walkingUp = new Animation();
	Animation walkingDown = new Animation();
	
	Animation idleRight = new Animation();
	Animation idleLeft = new Animation();
	Animation idleUp = new Animation();
	Animation idleDown = new Animation();
	
	public Rope(int x, int y, ENEMY_MOVE move, int live, TypeDrops drop, int droprate) {
		super(x, y, move, live, drop, droprate);
		this.original_posX = x;
		this.original_posY = y;
		
		this.texture = rope = new Texture("rope", original_posX, original_posY, original_posX+13, original_posY+16, "");
		rope.enemy = true;
		
		walkingRight.PushBack("assets/enemies/ropeR1.png",16,16);
		walkingRight.PushBack("assets/enemies/ropeR2.png",16,16);
		walkingRight.speed = 0.05f;
		
		walkingLeft.PushBack("assets/enemies/ropeL1.png",16,16);
		walkingLeft.PushBack("assets/enemies/ropeL2.png",16,16);
		walkingLeft.speed = 0.05f;
		
		walkingUp.PushBack("assets/enemies/ropeU1.png",12,16);
		walkingUp.PushBack("assets/enemies/ropeU2.png",12,16);
		walkingUp.speed = 0.05f;
		
		walkingDown.PushBack("assets/enemies/ropeD1.png",13,16);
		walkingDown.PushBack("assets/enemies/ropeD2.png",13,16);
		walkingDown.speed = 0.05f;
		
		idleDown.PushBack("assets/enemies/ratIdleD.png",12,16);
		idleUp.PushBack("assets/enemies/ratIdleU.png",12,16);
		idleRight.PushBack("assets/enemies/ratIdleR.png",15,16);
		idleLeft.PushBack("assets/enemies/ratIdleL.png",15,16);
		
		min_x = (original_posX*Main.size)-(100*Main.size);
		max_x = (original_posX*Main.size)+(100*Main.size);
		min_y = (original_posY*Main.size)-(100*Main.size);
		max_y = (original_posY*Main.size)+(100*Main.size);
		
		this.animationenemy = walkingDown;
	}

	public void Move() {
		switch(this.move) {
		case ROPE_V:
			Move_Rope_Verical();
			break;
		case ROPE_H:
			Move_Rope_Horizontal();
			break;
		default:
			break;
		}
		
	}

	private void Move_Rope_Horizontal() {
		int diffX = Main.App.player.link.x1 - rope.x1;
		int diffY = Main.App.player.link.y1 - rope.y1;
		
		if(diffX<300 && diffY<300 && diffX>-300 && diffY>-300) {
			activePath = true;
		}
		else {
			activePath = false;
		}
		if(activePath && flagMove) {
			cont++;
			if(cont < 50) {
				this.animationenemy = walkingRight;
				rope.x1 += speed;
				rope.x2 += speed;
			}
			else {
				this.animationenemy = walkingLeft;
				rope.x1 -= speed;
				rope.x2 -= speed;
				if(cont == 100) {
					cont = 0;
				}
			}
		}
	}

	private void Move_Rope_Verical() {
		int diffX = Main.App.player.link.x1 - rope.x1;
		int diffY = Main.App.player.link.y1 - rope.y1;
		
		if(diffX<300 && diffY<300 && diffX>-300 && diffY>-300) {
			activePath = true;
		}
		else {
			activePath = false;
		}
		if(activePath && flagMove) {
			cont++;
			if(cont < 50) {
				this.animationenemy = walkingDown;
				rope.y1 += speed;
				rope.y2 += speed;
			}
			else {
				this.animationenemy = walkingUp;
				rope.y1 -= speed;
				rope.y2 -= speed;
				if(cont == 100) {
					cont = 0;
				}
			}
		}
	}

	public void Collision() {
		if(activePath) {
			for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
				if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true) && rope.leftOn(Main.App.textures.sprites.get(i))) {
					rope.x1+=speed;
					rope.x2+=speed;
				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						 && rope.rightOn(Main.App.textures.sprites.get(i))) {
					rope.x1-=speed;
					rope.x2-=speed;

				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						 && rope.headOn(Main.App.textures.sprites.get(i))) {
					rope.y1+=speed;
					rope.y2+=speed;
				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						&& rope.stepsOn(Main.App.textures.sprites.get(i))) {
					rope.y1-=speed;
					rope.y2-=speed;
				}
			}
		}
	}
	
	@Override
	public boolean Death() {
		textdeath.x1 = rope.x1;
		textdeath.y1 = rope.y1;
		textdeath.changeFrame(deathAnimation.GetCurrentFrame(), deathAnimation.newWidth(), deathAnimation.newHeight());
		Main.App.textures.sprites.add(textdeath);
		
		if(deathAnimation.Finished()) {
			Main.App.entities.addDrop(rope.x1, rope.y1, drop, droprate);
			return true;
		}
		else {
			return false;
		}
		
	}

}