package Enemies;

import java.util.Random;

import Enums.Direction;
import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;
import Modules.ModuleEnemies.ENEMY_MOVE;
import Textures.Texture;

public class Rat extends Enemy {
	
	int original_posX;
	int original_posY;
	float speed = (float) (1.5 * Main.size);
	int cont = 0;
	int min_x, max_x, min_y, max_y;
	boolean activePath = false;
	
	Texture rat = null;
	Animation walkingRight = new Animation();
	Animation walkingLeft = new Animation();
	Animation walkingUp = new Animation();
	Animation walkingDown = new Animation();
	
	Animation idleRight = new Animation();
	Animation idleLeft = new Animation();
	Animation idleUp = new Animation();
	Animation idleDown = new Animation();
	
	Random r = new Random();
	
	Direction d = Direction.NONE;
	
	public Rat(int x, int y, ENEMY_MOVE move, int live, TypeDrops drop, int droprate) {
		super(x, y, move, live, drop, droprate);
		this.original_posX = x;
		this.original_posY = y;
		
		this.texture = rat = new Texture("rat", original_posX, original_posY, original_posX+12, original_posY+16, "");
		rat.enemy = true;
		
		walkingRight.PushBack("assets/enemies/ratR1.png",16,12);
		walkingRight.PushBack("assets/enemies/ratR2.png",16,12);
		walkingRight.speed = 0.05f;
		
		walkingLeft.PushBack("assets/enemies/ratL1.png",16,12);
		walkingLeft.PushBack("assets/enemies/ratL2.png",16,12);
		walkingLeft.speed = 0.05f;
		
		walkingUp.PushBack("assets/enemies/ratU1.png",12,16);
		walkingUp.PushBack("assets/enemies/ratU2.png",12,16);
		walkingUp.speed = 0.05f;
		
		walkingDown.PushBack("assets/enemies/ratD1.png",12,16);
		walkingDown.PushBack("assets/enemies/ratD2.png",12,16);
		walkingDown.speed = 0.05f;
		
		idleDown.PushBack("assets/enemies/ratIdleD.png",12,16);
		idleUp.PushBack("assets/enemies/ratIdleU.png",12,16);
		idleRight.PushBack("assets/enemies/ratIdleR.png",15,16);
		idleLeft.PushBack("assets/enemies/ratIdleL.png",15,16);
		
		min_x = (original_posX*Main.size)-(100*Main.size);
		max_x = (original_posX*Main.size)+(100*Main.size);
		min_y = (original_posY*Main.size)-(100*Main.size);
		max_y = (original_posY*Main.size)+(100*Main.size);
		
		
		d = newDirection();
		
		this.animationenemy = walkingDown;
	}
	
	private Direction newDirection() {
		int n = r.nextInt(4);
		switch(n) {
		case 0:
			return Direction.UP;
		case 1:
			return Direction.DOWN;
		case 2:
			return Direction.RIGHT;
		case 3:
			return Direction.LEFT;
		default:
			return Direction.NONE;
		}
	}

	public void Move() {
		switch(this.move) {
		case RAT_V:
			Move_Rat_Verical();
			break;
		case RAT_H:
			Move_Rat_Horizontal();
			break;
		case RAT_RANDOM:
			Move_Rat_Random();
			break;
		default:
			break;
		}
		
	}
	
	private void Move_Rat_Random() {
		int diffX = Main.App.player.link.x1 - rat.x1;
		int diffY = Main.App.player.link.y1 - rat.y1;
		
		if(diffX<150 && diffY<150 && diffX>-150 && diffY>-150) {
			activePath = true;
		}
		else {
			activePath=false;
		}
		if(flagMove && activePath) {
			cont++;
			if(cont < 75) {
				checkLimit();
				switch(d) {
				case UP:
					this.animationenemy = walkingUp;
					rat.y1 -= speed;
					rat.y2 -= speed;
					break;
				case DOWN:
					this.animationenemy = walkingDown;
					rat.y1 += speed;
					rat.y2 += speed;
					break;
				case RIGHT:
					this.animationenemy = walkingRight;
					rat.x1 += speed;
					rat.x2 += speed;
					break;
				case LEFT:
					this.animationenemy = walkingLeft;
					rat.x1 -= speed;
					rat.x2 -= speed;
					break;
				default:
					break;
				}
			}
			else {
				switch(d) {
				case UP:
					this.animationenemy = idleUp;
					break;
				case DOWN:
					this.animationenemy = idleDown;
					break;
				case RIGHT:
					this.animationenemy = idleRight;
					break;
				case LEFT:
					this.animationenemy = idleLeft;
					break;
				default:
					break;
				}
				if(cont > 100) {
					if(checkLimit())
						d = newDirection();
					cont = 0;
				}
			}
			
			if(cont == 30 || cont == 1) {
				Main.App.window.w.playSFX("assets/sfx/rat.wav");
			}
		}
		
	}
	
	private boolean checkLimit() {
		boolean flag = false;
		
		if(min_x > rat.x1) 
			d = Direction.RIGHT;
		else if(max_x < rat.x1)
			d = Direction.LEFT;
		else if (min_y > rat.y1)
			d = Direction.DOWN;
		else if (max_y < rat.y1)
			d = Direction.UP;
		else 
			flag = true;
		
		return flag;
	}

	private void Move_Rat_Horizontal() {
		int diffX = Main.App.player.link.x1 - rat.x1;
		int diffY = Main.App.player.link.y1 - rat.y1;
		
		if(diffX<150 && diffY<150 && diffX>-150 && diffY>-150) {
			activePath = true;
		}
		else {
			activePath=false;
		}
		if(flagMove && activePath) {
			cont++;
			if(cont < 50) {
				this.animationenemy = walkingRight;
				rat.x1 += speed;
				rat.x2 += speed;
			}
			else {
				this.animationenemy = walkingLeft;
				rat.x1 -= speed;
				rat.x2 -= speed;
				if(cont == 100) {
					cont = 0;
				}
			}
		}
	}

	private void Move_Rat_Verical() {
		int diffX = Main.App.player.link.x1 - rat.x1;
		int diffY = Main.App.player.link.y1 - rat.y1;
		
		if(diffX<150 && diffY<150 && diffX>-150 && diffY>-150) {
			activePath = true;
		}
		else {
			activePath=false;
		}
		if(flagMove && activePath) {
			cont++;
			if(cont < 50) {
				this.animationenemy = walkingDown;
				rat.y1 += speed;
				rat.y2 += speed;
			}
			else {
				this.animationenemy = walkingUp;
				rat.y1 -= speed;
				rat.y2 -= speed;
				if(cont == 100) {
					cont = 0;
				}
			}
		}
	}

	public void Collision() {
		if(activePath) {
			for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
				if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true) && rat.leftOn(Main.App.textures.sprites.get(i))) {
					rat.x1+=speed;
					rat.x2+=speed;
				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						 && rat.rightOn(Main.App.textures.sprites.get(i))) {
					rat.x1-=speed;
					rat.x2-=speed;

				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						 && rat.headOn(Main.App.textures.sprites.get(i))) {
					rat.y1+=speed;
					rat.y2+=speed;
				}
				else if((Main.App.textures.sprites.get(i).terrain || Main.App.textures.sprites.get(i).enemy == true)
						&& rat.stepsOn(Main.App.textures.sprites.get(i))) {
					rat.y1-=speed;
					rat.y2-=speed;
				}
			}
		}
	}
	
	@Override
	public boolean Death() {
		textdeath.x1 = rat.x1;
		textdeath.y1 = rat.y1;
		textdeath.changeFrame(deathAnimation.GetCurrentFrame(), deathAnimation.newWidth(), deathAnimation.newHeight());
		Main.App.textures.sprites.add(textdeath);
		
		if(deathAnimation.Finished()) {
			Main.App.entities.addDrop(rat.x1, rat.y1, drop, droprate);
			return true;
		}
		else {
			return false;
		}
		
	}

}
