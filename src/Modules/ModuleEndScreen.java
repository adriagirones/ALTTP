package Modules;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Textures.Text;
import Textures.Texture;

public class ModuleEndScreen extends Module{
	
	Texture background;
	
	File f = new File("files/save");
	File faux = new File("files/save");
	Texture arrow;
	boolean accept = false;
	String nick ="";
	String[][] mat = {{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"}, 
					  {"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T"},
					  {"U", "V", "W", "X", "Y", "Z", ".", ",", " ", " "}};
	int matx=0, maty=0;
	Text name;
	
	/**
	 * Default ModuleEndScreen() constructor.
	 */
	ModuleEndScreen() {

	}
	
	boolean Start() {
		Main.App.window.w.playMusic("assets/music/ending.wav");
		nick ="";
		Color back = new Color(40, 32, 32);
		Main.App.window.f.setBackground(back);
		
		background = new Texture("fondo", 0, 0, 260, 256, "assets/backgrounds/endscreen.png");
		arrow = new Texture("fondo", 25, 127, 25+6, 127+9, "assets/backgrounds/menuarrow.png");
		
		name = new Text("name", 42, 105, nick);
		name.textColor = 0xe6e6e6;
		name.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 16);
		
		
		return true;
	}
	
	boolean CleanUp() {
		if(Main.App.window.f != null) {
			Main.App.window.f.resetScroll();
		}	
		
		return true;
	}
	
	Main.update_status Update() {
		Main.App.textures.sprites.add(background);
		Main.App.textures.sprites.add(arrow);
		Main.App.textures.sprites.add(name);
		
		if(Main.App.input.keysDown.contains('d')) {
			Main.App.window.w.playSFX("assets/sfx/cursor.wav");
			maty++;
			if(maty==10) {
				maty=0;
			}
		}
		else if(Main.App.input.keysDown.contains('a')) {
			Main.App.window.w.playSFX("assets/sfx/cursor.wav");
			maty--;
			if(maty==-1) {
				maty=9;
			}
		}
		else if(Main.App.input.keysDown.contains('w')) {
			Main.App.window.w.playSFX("assets/sfx/cursor.wav");
			matx--;
			if(matx==-1) {
				matx=2;
			}
		}
		else if(Main.App.input.keysDown.contains('s')) {
			Main.App.window.w.playSFX("assets/sfx/cursor.wav");
			matx++;
			if(matx==3) {
				matx=0;
			}
		}
		
		switch(matx) {
		case 0:
			arrow.y1=127*Main.size; arrow.y2=(127+9)*Main.size;
			arrow.x1=(25+(16*maty))*Main.size; arrow.x2=((25+(16*maty))+6)*Main.size;
			break;
		case 1:
			arrow.y1=143*Main.size; arrow.y2=(143+9)*Main.size;
			arrow.x1=(25+(16*maty))*Main.size; arrow.x2=((25+(16*maty))+6)*Main.size;
			break;
		case 2:
			arrow.y1=159*Main.size; arrow.y2=(159+9)*Main.size;
			if(maty==9) {
				arrow.x1=192*Main.size; arrow.x2=(192+6)*Main.size;
				arrow.y1=182*Main.size; arrow.y2=(182+9)*Main.size;
			}
			else {
				arrow.x1=(25+(16*maty))*Main.size; arrow.x2=((25+(16*maty))+6)*Main.size;
			}
			break;
		}
		
		
		if(Main.App.input.keysDown.contains(' ')) {
			if(matx==2 && maty==9) {
				accept=true;
				matx=0; maty=0;
				Main.App.window.w.playSFX("assets/sfx/message finish.wav");
			}
			else if(matx==2 && maty==8) {
				nick = nick.substring(0, nick.length()-1);
				Main.App.window.w.playSFX("assets/sfx/enemy hit.wav");
			}
			else{
				if(nick.length()<7) {
					nick += mat[matx][maty];
					Main.App.window.w.playSFX("assets/sfx/message.wav");
				}
				else {
					Main.App.window.w.playSFX("assets/sfx/error.wav");
				}
			}
		}
		
		System.out.println(nick);
		System.out.println(matx+" - "+maty);
		
		name.path=nick;
		
		if(accept) {
			accept = false;
			ArrayList<String> save = new ArrayList<String>();
			try {
				BufferedReader br = new BufferedReader(new FileReader(f));
				try {
					while(br.ready()) {
						save.add(br.readLine());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			
			System.out.println(save);
			
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(f));
				for (int i = 0; i < save.size(); i++) {
					bw.write(save.get(i));
					bw.newLine();
				}
				bw.write(nick+"-"+String.valueOf(Main.App.player.timerecord)+"-"+String.valueOf(Main.App.player.numRupees)+"\n");
				bw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Main.App.fade.FadeToBlack(this, Main.App.menu, (float) 1.8);
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}


}