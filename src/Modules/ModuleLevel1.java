package Modules;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import Enums.TreasureReward;
import Enums.TypeDrops;
import Textures.Pot;
import Textures.Texture;
import Textures.Treasure;
import Textures.Wall;

/**
 * Module of the level1.
 * @author Adria Girones Calzada
 *
 */
public class ModuleLevel1 extends Module implements Level {
	
	Texture background;
	boolean wallMovingActived = false;
	
	public ArrayList<Wall> walls = new ArrayList<>();
	ArrayList<Wall> hallWays = new ArrayList<>();
	ArrayList<Wall> brokenDoor = new ArrayList<>();
	
	ArrayList<Pot> pots = new ArrayList<>();
	ArrayList<Treasure> treasures = new ArrayList<>();
	
	Texture tolevel0;
	Texture tolevel2;
	public boolean fromlevel0 = false;
	public boolean fromlevel2 = false;
	
	/**
	 * Default ModuleLevel1() constructor.
	 */
	ModuleLevel1() {
		
	}
	
	boolean Start() {
		Main.App.window.w.playMusic("assets/music/cave.wav");
		Color back = new Color(40, 32, 32);
		Main.App.window.f.setBackground(back);
		
		InitializeCamera();
		
		Main.App.player.Enable();
		Main.App.ui.Enable();
		Main.App.enemies.Enable();
		Main.App.entities.Enable();
			
		background = new Texture("fondo", 0, 0, 1800, 1990, "assets/backgrounds/P1v3.png");
		tolevel0 = new Texture("connection", 731, 1903, 731+80, 1903+87, "assets/backgrounds/P1enter.png");		
		tolevel2 = new Texture("connection", 545, 76, 545+22, 76+40, "assets/backgrounds/P1toP2.png");		
		
		//ENEMIES
		
		//WALLS
		walls.add(new Wall(731, 1821, 28, 92));
		walls.add(new Wall(370, 1821, 389, 27));
		walls.add(new Wall(370, 1433, 30, 415));
		walls.add(new Wall(370, 1433, 243, 24));
		walls.add(new Wall(586, 1433, 27, 169));
		walls.add(new Wall(586, 1578, 233, 24));
		
		walls.add(new Wall(783, 1821, 28, 92));
		walls.add(new Wall(783, 1821, 100, 28));
		walls.add(new Wall(855, 1770, 27, 78));
		walls.add(new Wall(423, 1770, 461, 23));
		walls.add(new Wall(423, 1486, 28, 307));
		walls.add(new Wall(423, 1486, 137, 27));
		walls.add(new Wall(533, 1486, 28, 171));
		walls.add(new Wall(533, 1630, 662, 27));
		walls.add(new Wall(1167, 1630, 28, 161));
		walls.add(new Wall(1023, 1767, 172, 25));
		walls.add(new Wall(1023, 1767, 28, 79));
		walls.add(new Wall(1023, 1820, 583, 26));
		walls.add(new Wall(1578, 1646, 28, 200));
		
		walls.add(new Wall(1527, 1646, 28, 145));
		walls.add(new Wall(1219, 1767, 336, 24));
		walls.add(new Wall(1219, 1044, 28, 747));
		walls.add(new Wall(1219, 1044, 369, 28));
		walls.add(new Wall(1560, 862, 29, 211));
		walls.add(new Wall(1509, 862, 79, 24));
		walls.add(new Wall(1509, 862, 28, 153));
		walls.add(new Wall(1218, 992, 320, 23));
		walls.add(new Wall(1218, 883, 28, 132));
		walls.add(new Wall(1218, 883, 172, 27));
		walls.add(new Wall(1364, 247, 26, 663));
		walls.add(new Wall(1364, 247, 158, 29));
		walls.add(new Wall(1494, 247, 28, 173));
		walls.add(new Wall(1494, 392, 79, 28));
		walls.add(new Wall(1545, 195, 28, 225));
		walls.add(new Wall(1310, 195, 263, 25));
		walls.add(new Wall(1310, 195, 28, 50-1));
		
		walls.add(new Wall(1311, 273-3, 28, 581+3));
		walls.add(new Wall(1167, 830, 172, 24));
		walls.add(new Wall(1167, 830, 27, 186));
		walls.add(new Wall(658, 992, 536, 24));
		walls.add(new Wall(658, 992, 28, 160));
		walls.add(new Wall(555, 1129, 131, 23));
		walls.add(new Wall(555, 1129, 28, 161));
		walls.add(new Wall(294, 1266, 290-2, 24));
		walls.add(new Wall(294, 1075, 27, 215));
		walls.add(new Wall(242, 1075, 79, 24));
		walls.add(new Wall(242, 1075, 28, 215));
		walls.add(new Wall(135, 1266, 135, 24));
		walls.add(new Wall(135, 747, 28, 543));
		walls.add(new Wall(135, 747, 348, 28));
		
		walls.add(new Wall(515, 747, 229, 28));
		walls.add(new Wall(717, 747, 28, 172));
		walls.add(new Wall(717, 892, 241, 27));
		walls.add(new Wall(932, 505, 26, 414));
		walls.add(new Wall(571, 505, 387, 23));
		walls.add(new Wall(571, 440, 28, 88));
		
		walls.add(new Wall(519, 440, 28, 88));
		walls.add(new Wall(447, 504, 100, 24));
		walls.add(new Wall(447, 504, 28, 79));
		walls.add(new Wall(447, 556, 460, 27));
		walls.add(new Wall(879, 556, 28, 308));
		walls.add(new Wall(770, 840, 137, 24));
		walls.add(new Wall(770, 696, 27, 168));
		walls.add(new Wall(135, 696, 662, 23));
		walls.add(new Wall(135, 558, 28, 161));
		walls.add(new Wall(135, 558, 172, 28));
		walls.add(new Wall(280, 507, 27, 79));
		walls.add(new Wall(82, 507, 226, 23));
		walls.add(new Wall(82, 507, 29, 838));
		walls.add(new Wall(82, 1318, 552, 27));
		walls.add(new Wall(606, 1182, 28, 163));
		walls.add(new Wall(606, 1182, 131, 27));
		walls.add(new Wall(709, 1045, 28, 164));
		walls.add(new Wall(709, 1045, 289, 27));
		walls.add(new Wall(971, 1045, 27, 218));
		walls.add(new Wall(971, 1235, 79, 28));
		walls.add(new Wall(1022, 1044, 28, 219));
		walls.add(new Wall(1022, 1044, 173, 27));
		walls.add(new Wall(1167, 1044, 28, 558));
		walls.add(new Wall(844, 1578, 347, 24));
		
		walls.add(new Wall(957, 179, 184, 24));
		walls.add(new Wall(957, 179, 28, 224));
		walls.add(new Wall(957, 375, 184, 28));
		walls.add(new Wall(1113, 267, 28, 136));
		walls.add(new Wall(1113, 179, 28, 63));
		
		walls.add(new Wall(390, 895, 98, 24));
		walls.add(new Wall(390, 895, 28, 87));
		walls.add(new Wall(390, 954, 160, 28));
		walls.add(new Wall(522, 895, 28, 87));
		walls.add(new Wall(510, 895, 40, 25));
		
		walls.add(new Wall(780, 1371, 28, 87));
		walls.add(new Wall(779, 1371, 161, 24 ));
		walls.add(new Wall(912, 1371, 28, 87));
		walls.add(new Wall(842, 1430, 98, 28));
		walls.add(new Wall(780, 1430, 41, 29));
		
		walls.add(new Wall(1431, 1158, 28, 375));
		walls.add(new Wall(1431, 1158, 272, 24));
		walls.add(new Wall(1675, 1158, 28, 375));
		walls.add(new Wall(1579, 1505, 124, 28));
		walls.add(new Wall(1429, 1505, 126, 28));
		
		walls.add(new Wall(571, 286, 122, 29));
		walls.add(new Wall(672, 100, 21, 215));
		walls.add(new Wall(572, 100, 121, 22));
		walls.add(new Wall(421, 100, 119, 22));
		walls.add(new Wall(421, 101, 23, 214));
		walls.add(new Wall(421, 291, 126, 23));
		
		hallWays.add(new Wall(819, 1447, 24, 142, "assets/backgrounds/P1a.png"));
		hallWays.add(new Wall(1556, 1521, 24, 134, "assets/backgrounds/P1b.png"));
		hallWays.add(new Wall(487, 764, 24, 142, "assets/backgrounds/P1c.png"));
		hallWays.add(new Wall(547, 303, 23, 149, "assets/backgrounds/P1d.png"));
		hallWays.add(new Wall(1127, 244, 194, 24, "assets/backgrounds/P1e.png"));
		
		brokenDoor.add(new Wall(819, 1578, 28, 24, "assets/backgrounds/P1brokena.png"));
		brokenDoor.add(new Wall(1554, 1646, 25, 25, "assets/backgrounds/P1brokenb.png"));
		brokenDoor.add(new Wall(487, 750-3, 25, 25+3, "assets/backgrounds/P1brokenc.png"));
		brokenDoor.add(new Wall(547, 440, 25, 29, "assets/backgrounds/P1brokene.png"));
		brokenDoor.add(new Wall(1310, 244, 30, 25, "assets/backgrounds/P1brokend.png"));
		
		pots.add(new Pot(890, 1407, TypeDrops.RUPEE_BLUE));
		pots.add(new Pot(1542, 897, TypeDrops.BOMBS3));
		pots.add(new Pot(1528, 367, TypeDrops.NONE));
		pots.add(new Pot(276, 1110, TypeDrops.BOMBS3));
		pots.add(new Pot(487, 537, TypeDrops.BOMBS3));
		pots.add(new Pot(428, 931, TypeDrops.RUPEE_RED));
		
		treasures.add(new Treasure("treasure", 996, 213, TreasureReward.HEARTCONTAINER));
		
		//ENEMIES
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 1473, 1216 ,1, TypeDrops.NONE, 10);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 1638, 1218 ,1, TypeDrops.HEART, 10);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 1569, 1216 ,1, TypeDrops.NONE, 10);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.ROPE, ModuleEnemies.ENEMY_MOVE.ROPE_H, 494, 1801 ,1, TypeDrops.RUPEE_GREEN, 7);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.KEESE, ModuleEnemies.ENEMY_MOVE.KEESE_V, 404, 1515 ,1, TypeDrops.RUPEE_GREEN, 6);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.KEESE, ModuleEnemies.ENEMY_MOVE.KEESE_V, 1200, 1394 ,1, TypeDrops.RUPEE_GREEN, 5);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.KEESE, ModuleEnemies.ENEMY_MOVE.KEESE_V, 1198, 895 ,1, TypeDrops.HEART, 7);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.ROPE, ModuleEnemies.ENEMY_MOVE.ROPE_V, 1344, 380 ,1, TypeDrops.RUPEE_GREEN, 8);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.ROPE, ModuleEnemies.ENEMY_MOVE.ROPE_H, 771, 1025 ,1, TypeDrops.RUPEE_GREEN, 9);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.ROPE, ModuleEnemies.ENEMY_MOVE.ROPE_H, 260, 1298 ,1, TypeDrops.HEART, 8);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.KEESE, ModuleEnemies.ENEMY_MOVE.KEESE_V, 116, 789 ,1, TypeDrops.RUPEE_GREEN, 6);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.KEESE, ModuleEnemies.ENEMY_MOVE.KEESE_V, 115, 669 ,1, TypeDrops.RUPEE_GREEN, 6);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.ROPE, ModuleEnemies.ENEMY_MOVE.ROPE_V, 914, 692 ,1, TypeDrops.HEART, 7);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 473, 162 ,1, TypeDrops.HEART, 8);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 599, 221 ,1, TypeDrops.RUPEE_GREEN, 8);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 1028, 292 ,1, TypeDrops.RUPEE_BLUE, 2);

		
		return true;
	}
	
	@Override
	public void InitializeCamera() {
		if(Main.size == 2) {
			if(fromlevel0) {
				Main.App.window.f.scroll(-640*Main.size, -1775*Main.size);
			}
			else if(fromlevel2) {
				Main.App.window.f.scroll(-423*Main.size, -15*Main.size);
			}
		}
	}
	
	boolean CleanUp() {
		Main.App.ui.Disable();
		Main.App.enemies.Disable();
		Main.App.player.Disable();
		Main.App.entities.Disable();
		
		if(Main.App.window.f != null) {
			Main.App.window.f.resetScroll();
		}
			
		return true;
	}
	
	Main.update_status Update() {
		
		Main.App.textures.sprites.add(background);
		Main.App.textures.sprites.addAll(walls);
		
//		if(Main.App.input.keysDown.contains('9')) {
//			Main.App.fade.FadeToBlack(this, Main.App.background, (float) 1.8);
//		}
		
		if(Main.App.player.link.collidesWith(tolevel0)) {
			Main.App.fade.FadeToBlack(this, Main.App.level0, (float) 1.8);
			Main.App.level0.fromlevel1=true;
			Main.App.player.flagMove=false;
		}
		
		if(Main.App.player.link.collidesWith(tolevel2) || (Main.App.input.keysDown.contains('8') && Main.App.input.modeDebug)) {
			Main.App.fade.FadeToBlack(this, Main.App.level2, (float) 1.8);
			Main.App.level2.fromlevel1=true;
			Main.App.player.flagMove=false;
		}
		
		destroyedDoors();
		UpdatePots(pots);
		UpdateTreasures(treasures);
		
		return Main.update_status.UPDATE_CONTINUE;
	}

	Main.update_status PostUpdate() {
		
		for (int i = 0; i < hallWays.size(); i++) {
			if(hallWays.get(i).autoMoveActive) {
				Main.App.textures.sprites.add(hallWays.get(i));
			}
		}
		
		autoMove(hallWays);
		Main.App.textures.sprites.add(tolevel0);
		Main.App.textures.sprites.add(tolevel2);
				
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Function that checks doors destroyed by bombs.
	 */
	private void destroyedDoors() {
		for (int i = 0; i < brokenDoor.size(); i++) {
			if(brokenDoor.get(i).destroyed==false) {
				Main.App.textures.sprites.add(brokenDoor.get(i));
				for (int j = 0; j < Main.App.player.bombDamage.size(); j++) {
					if(Main.App.player.bombDamage.get(j).collidesWith(brokenDoor.get(i))) {
						brokenDoor.get(i).destroyed=true;
						hallWays.get(i).autoMoveActive=true;
					}
				}
			}
		}
	}

	/**
	 * Function that moves the character between rooms on the map
	 * @param hallWays connection between two rooms
	 */
	private void autoMove(ArrayList<Wall> hallWays) {
		for(Wall texture : hallWays) {
			if(texture.autoMoveActive) {
				if(Main.App.player.link.headOn(texture) && !wallMovingActived) {
					Main.App.player.flagMove=false;
					wallMovingActived = true;
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							Main.App.player.link.y1-=5*Main.size;
							Main.App.player.link.y2-=5*Main.size;
							Main.App.window.f.scroll(0, 5*Main.size);
							if(!Main.App.player.link.collidesWith(texture)) {
								Main.App.player.flagMove=true;
								Main.App.player.link.y1-=5*Main.size;
								Main.App.player.link.y2-=5*Main.size;
								Main.App.window.f.scroll(0, 5*Main.size);
								wallMovingActived = false;
								timer.cancel();
							}
						}
					}, 0, 30);
				}
				else if(Main.App.player.link.stepsOn(texture) && !wallMovingActived) {
					Main.App.player.flagMove=false;
					wallMovingActived = true;
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							Main.App.player.link.y1+=5*Main.size;
							Main.App.player.link.y2+=5*Main.size;
							Main.App.window.f.scroll(0, -5*Main.size);
							if(!Main.App.player.link.collidesWith(texture)) {
								Main.App.player.flagMove=true;
								Main.App.player.link.y1+=5*Main.size;
								Main.App.player.link.y2+=5*Main.size;
								Main.App.window.f.scroll(0, -5*Main.size);
								timer.cancel();
								wallMovingActived = false;
							}
						}
					}, 0, 30);
				}
				else if(Main.App.player.link.rightOn(texture) && !wallMovingActived) {
					Main.App.player.flagMove=false;
					wallMovingActived = true;
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							Main.App.player.link.x1+=5*Main.size;
							Main.App.player.link.x2+=5*Main.size;
							Main.App.window.f.scroll(-5*Main.size, 0);
							if(!Main.App.player.link.collidesWith(texture)) {
								Main.App.player.flagMove=true;
								Main.App.player.link.x1+=5*Main.size;
								Main.App.player.link.x2+=5*Main.size;
								Main.App.window.f.scroll(-5*Main.size, 0);
								timer.cancel();
								wallMovingActived = false;
							}
						}
					}, 0, 30);
				}
				else if(Main.App.player.link.leftOn(texture) && !wallMovingActived) {
					Main.App.player.flagMove=false;
					wallMovingActived = true;
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							Main.App.player.link.x1-=5*Main.size;
							Main.App.player.link.x2-=5*Main.size;
							Main.App.window.f.scroll(5*Main.size, 0);
							if(!Main.App.player.link.collidesWith(texture)) {
								Main.App.player.flagMove=true;
								Main.App.player.link.x1-=5*Main.size;
								Main.App.player.link.x2-=5*Main.size;
								Main.App.window.f.scroll(5*Main.size, 0);
								timer.cancel();
								wallMovingActived = false;
							}
						}
					}, 0, 30);
				}
			}
		}
	}

}
