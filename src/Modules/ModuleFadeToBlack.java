package Modules;

import java.awt.Color;
import Core.Sprite;

/**
 * Module that allows to make a fade to black between two modules, disabling one and enabling the other.
 * @author Adria Girones Calzada
 *
 */
public class ModuleFadeToBlack extends Module {
	
	/**
	 * Module that will be enabled.
	 */
	private Module on = null;
	
	/**
	 * Module that will be disabled.
	 */
	private Module off = null;
	
	/**
	 * Time in milliseconds when the fade to black has begun.
	 */
	private float start_time;
	
	/**
	 * Time in milliseconds when to end the fade to black.
	 */
	private float total_time;
	
	/**
	 * Statements which may be the fade to black.
	 */
	private enum fade_step {
		none,
		fade_to_black,
		fade_from_black;
	} 
	
	/**
	 * Current step of fade to black.
	 */
	private fade_step current_step = fade_step.none;
	
	/**
	 * Time variable.
	 */
	private Time ticks = new Time();
	
	/**
	 * Default ModuleFadeToBlack() constructor.
	 */
	public ModuleFadeToBlack() {
		
	}
	
	boolean Start() {
		ticks.startMilisec();;
		return true;
	}
	
	Main.update_status PostUpdate() {
		
		if(current_step == fade_step.none) {
			return Main.update_status.UPDATE_CONTINUE;
		}
		
		float now = (float) (ticks.tickCountInMiliseconds() - start_time);
		float normalized = Min(1.0f, ((float)now/(float)total_time));
		
		switch(current_step) {
		case fade_to_black:
			if(now >= total_time) {
				off.Disable();
				on.Enable();
				total_time += total_time;
				start_time = (long) ticks.tickCountInMiliseconds();
				current_step = fade_step.fade_from_black;
			}
			break;
		case fade_from_black:
			normalized = 1.0f - normalized;
			if(now>=total_time) {
				current_step = fade_step.none;
			}
			break;
		default:
			break;
		}
		
		Color alpha = new Color(0, 0, 0, (int)(normalized*255.0f));
		Sprite fadeTexture= new Sprite("fade", 0, 0, 1024, 1024, 0, alpha);
		fadeTexture.unscrollable = true;
		Main.App.textures.sprites.add(fadeTexture);
		
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Makes a fade to black between two modules. One must be enabled and the other disabled.
	 * @param module_off Module to be disabled.
	 * @param module_on Module to be enabled.
	 * @param sec Number of seconds the fade to black will last.
	 * @return Returns true if there was no error.
	 */
	boolean FadeToBlack(Module module_off, Module module_on, float sec) {
		boolean ret = false;
		if(current_step == fade_step.none) {
			current_step = fade_step.fade_to_black;
			start_time = (float) ticks.tickCountInMiliseconds();
			total_time = (float) (sec * 0.5f * 1000.0f);
			on = module_on;
			off = module_off;
			ret = true;
		}
		
		return ret;
	}
	
	/**
	 * Says if the module is doing a fade to black right now.
	 * @return Return true if the module is doing a fade to black.<br>
	 * Return false if the module is not doing a fade to black.
	 */
	boolean IsFading() {
		return current_step != fade_step.none;
	}
	
	/**
	 * Function that returns the minimum number.
	 * @param a float number
	 * @param b float number
	 * @return Returns the minimum number.
	 */
	private float Min(float a, float b) {
		if(a<b) {
			return a;
		}
		else {
			return b;
		}
	}

}
