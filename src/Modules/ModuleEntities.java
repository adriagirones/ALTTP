package Modules;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Enums.TypeDrops;
import Textures.Drop;
import Textures.Heart;
import Textures.Rupee;
import Textures.BombDrop;

/**
 * Module where the drops are created.
 * @author Adria Girones Calzada
 *
 */
public class ModuleEntities extends Module {
	
	Random r = new Random();
	
	/**
	 * List of the drops that will be printed
	 */
	public ArrayList<Drop> drops = new ArrayList<>();
	
	/**
	 * Default ModuleEntities() constructor.
	 */
	ModuleEntities() {
		
	}
	
	boolean Start() {
		return true;
	}
	
	Main.update_status Update() {
		for (int i = 0; i < drops.size(); i++) {
			drops.get(i).changePath();
		}
		Main.App.textures.sprites.addAll(drops);
		checkCollision();
		
		return Main.update_status.UPDATE_CONTINUE;
	}

	boolean CleanUp() {
		drops.clear();
		return true;
	}
	
	/**
	 * Check if the character or his sword collides with an entity. 
	 * If this happens, depending on each type it will call different functions.
	 */
	private void checkCollision() {
		ArrayList<Drop> tempdrops = new ArrayList<>();
		for (int i = 0; i < drops.size(); i++) {
			if(Main.App.player.link.collidesWith(drops.get(i)) || Main.App.player.link.sword.collidesWith(drops.get(i))) {
				if(drops.get(i) instanceof Rupee) {
					Main.App.window.w.playSFX("assets/sfx/rupee.wav");
					if(drops.get(i).type == TypeDrops.RUPEE_GREEN) {
						Main.App.player.numRupees++;
					}
					else if(drops.get(i).type == TypeDrops.RUPEE_BLUE) {
						Main.App.player.numRupees+=5;
					}
					else if(drops.get(i).type == TypeDrops.RUPEE_RED) {
						Main.App.player.numRupees+=20;
					}
				}
				else if(drops.get(i) instanceof Heart) {
					Main.App.player.moreLive();
					Main.App.window.w.playSFX("assets/sfx/heart.wav");
				}
				else if(drops.get(i) instanceof BombDrop) {
					Main.App.player.numBombs+=3;
				}
			}
			else {
				tempdrops.add(drops.get(i));
			}
		}
		drops.clear();
		drops.addAll(tempdrops);
	}
	
	/**
	 * Add a drop in the list of printable sprite
	 * @param x1 Horizontal position where the drop will be created.
	 * @param y1 Vertical position where the drop will be created.
	 * @param drop Type of the drop.
	 * @param droprate Drop rate.
	 */
	public void addDrop(int x1, int y1, TypeDrops drop, int droprate) {
		Timer timer = new Timer();
		int n = r.nextInt(10);
		if(n < droprate) {
			timer.schedule(new TimerTask() {
				int count = 0;
				public void run() {
					switch(drop) {
					case RUPEE_GREEN:
						drops.add(new Rupee("rupee", x1/Main.size, y1/Main.size, TypeDrops.RUPEE_GREEN));
						break;
					case RUPEE_BLUE:
						drops.add(new Rupee("rupee", x1/Main.size, y1/Main.size, TypeDrops.RUPEE_BLUE));
						break;
					case RUPEE_RED:
						drops.add(new Rupee("rupee", x1/Main.size, y1/Main.size, TypeDrops.RUPEE_RED));
						break;
					case HEART:
						drops.add(new Heart("heart", x1/Main.size, y1/Main.size, TypeDrops.HEART));
						break;
					case BOMBS3:
						drops.add(new BombDrop("bombdrop", x1/Main.size, y1/Main.size, TypeDrops.BOMBS3));
						break;
					default:
						break;
					}
					if(++count == 1) {
						timer.cancel();
					}
				}
			}, 350, 150);
		}	
	}
}
