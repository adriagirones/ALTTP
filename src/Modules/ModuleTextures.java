package Modules;

import java.util.ArrayList;

import Core.Sprite;

/**
 * Module where the sprite list is created.
 * @author Adria Girones Calzada
 *
 */
public class ModuleTextures extends Module {
	
	/**
	 * List with all sprite that be must render.
	 */
	public ArrayList<Sprite> sprites = new ArrayList<>();
	
	/**
	 * Default ModuleTextures() constructor.
	 */
	ModuleTextures() {
		
	}
	
	boolean Init() {
		return true;
	}
	
	boolean CleanUp() {
		return true;
	}

}
