package Modules;

import java.awt.Toolkit;

import Core.Field;
import Core.Window;

/**
 * Module where the window and field are created.
 * @author Adria Girones Calzada
 *
 */
public class ModuleWindow extends Module {
	
	/**
	 * Pixel Field.
	 */
	public Field f;
	/**
	 * Window of the game.
	 */
	public Window w;
	
	/**
	 * Default ModuleWindow() constructor.
	 */
	ModuleWindow() {
		
	}
	
	boolean Init() {
		f = new Field();
		w = new Window(f);
		w.setIconImage(Toolkit.getDefaultToolkit().getImage("assets/icon.png"));
		w.setTitle("The Legend of Zelda");
		return true;
	}

}
