package Modules;

import java.util.ArrayList;

import Enemies.Enemy;
import Enemies.Keese;
import Enemies.Rat;
import Enemies.Rope;
import Enemies.SwordSoldier;
import Enums.Direction;
import Enums.TypeDrops;

/**
 * Module where enemies are created.
 * @author Adria Girones Calzada
 *
 */
public class ModuleEnemies extends Module {
	
	/**
	 * List with all active enemies.
	 */
	public ArrayList<Enemy> enemies = new ArrayList<>();

	/**
	 * Enumeration of all types of enemies.
	 */
	public enum ENEMY_TYPES {
		NO_TYPE,
		SWORD_SOLDIER,
		RAT,
		ROPE,
		KEESE
	};
	
	/**
	 * Enumeration of all enemies moves.
	 */
	public enum ENEMY_MOVE {
		NO_MOVE,
		SWORD_SOLDIER_1,
		SWORD_SOLDIER_2,
		RAT_H,
		RAT_V,
		RAT_RANDOM,
		ROPE_H,
		ROPE_V,
		KEESE_H,
		KEESE_V
	};
	
	/**
	 * Default ModuleEnemies() constructor.
	 */
	ModuleEnemies() {
			
	}
	
	boolean Start() {
		return true;
	}
	
	Main.update_status Update() {
		
		for (int i = 0; i < enemies.size(); ++i) {
			if(enemies.get(i).live > 0) {
				enemies.get(i).texture.changeFrame(enemies.get(i).animationenemy.GetCurrentFrame(), enemies.get(i).animationenemy.newWidth(), enemies.get(i).animationenemy.newHeight());
				enemies.get(i).texture.changeImage(enemies.get(i).animationenemy.GetCurrentFrame());
				Main.App.textures.sprites.add(enemies.get(i).texture);
			}
		}
		
		ArrayList<Enemy> provisionalenemies = new ArrayList<>();
		
		for (int i = 0; i < enemies.size(); ++i) {
			provisionalenemies.add(enemies.get(i));
			enemies.get(i).Move();
			enemies.get(i).Collision();
			if(enemies.get(i).live == 0) {
				enemies.get(i).flagMove=false;
				if(enemies.get(i).Death()) {
					Main.App.window.w.playSFX("assets/sfx/enemy dies.wav");
					provisionalenemies.remove(provisionalenemies.size()-1);
				}
			}
		}
		
		enemies.clear();
		enemies.addAll(provisionalenemies);
			
		checkEnemyCollision(Main.App.player);
		
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	boolean CleanUp() {
		enemies.clear();
		return true;
	}
	
	/**
	 * Add an enemy.
	 * @param type Which enemy is it.
	 * @param move Which move will it has.
	 * @param x1 Horizontal position where the enemy will be created.
	 * @param y1 Vertical position where the enemy will be created.
	 * @param live Live of the enemy.
	 * @param drop Type of the drop.
	 * @param droprate Drop rate.
	 * @return Returns true if there was no error.
	 */
	public boolean AddEnemy(ENEMY_TYPES type, ENEMY_MOVE move, int x1, int y1, int live, TypeDrops drop, int droprate) {
		boolean ret = false;
		switch(type) {
		case SWORD_SOLDIER:
			enemies.add(new SwordSoldier(x1,y1,move,live,drop,droprate));
			break;
		case RAT:
			enemies.add(new Rat(x1,y1,move,live,drop,droprate));
			break;
		case ROPE:
			enemies.add(new Rope(x1,y1,move,live,drop,droprate));
			break;
		case KEESE:
			enemies.add(new Keese(x1,y1,move,live,drop,droprate));
			break;
		default:
			break;
		}
		return ret;
	}
	
	/**
	 * Check if the character has collided with an enemy.
	 * @param player The player with whom we checked the collision.
	 */
	public void checkEnemyCollision(ModulePlayer player) {
		for (int i = 0; i < Main.App.textures.sprites.size() && !player.dead && !player.invencible; i++) {
			if(Main.App.textures.sprites.get(i).enemy == true && player.link.collisionBox.headOn(Main.App.textures.sprites.get(i))) {
				player.hitPlayer(Direction.DOWN, player.link.knockbackDown, player.link.idleUp);
			}
			else if(Main.App.textures.sprites.get(i).enemy == true && player.link.collisionBox.stepsOn(Main.App.textures.sprites.get(i))) {
				player.hitPlayer(Direction.UP, player.link.knockbackUp, player.link.idleDown);
			}
			else if(Main.App.textures.sprites.get(i).enemy == true && player.link.collisionBox.rightOn(Main.App.textures.sprites.get(i))) {
				player.hitPlayer(Direction.RIGHT, player.link.knockbackRight, player.link.idleLeft);
			}
			else if(Main.App.textures.sprites.get(i).enemy == true && player.link.collisionBox.leftOn(Main.App.textures.sprites.get(i))) {
				player.hitPlayer(Direction.LEFT, player.link.knockbackLeft, player.link.idleRight);
			}
		}
	}

}
