package Modules;

/**
 * Calculates the current time.
 * @author Adria Girones Calzada
 *
 */
public class Time {
	
	private static long _firstCallTimeSeconds = 0;
	public long start = 0;
	private double startMsec = 0;
	private boolean flag = false;
	
	/**
	 * Default Time constructor.
	 */
	Time() {
		
	}
	
	/**
	 * Initialize the counter
	 */
	public void startSeconds() {
		if(!flag) {
			start = System.currentTimeMillis() / 1000L;
			flag = true;
		}
		 
	}
	
	/**
	 * Initialize the counter
	 */
	public void startMilisec() {
		if(!flag) {
			startMsec = System.currentTimeMillis();
			flag = true;
		}	 
	}
	
	/**
	 * Function that checks if have passed the seconds specified since the function startSeconds() function was called.
	 * @param seconds that we want to check
	 * @return Returns true if the specified seconds have passed.
	 * Returns false Returns true if the specified seconds have not passed.
	 */
	public boolean countSeconds(int seconds) {
		if(tickCountInSeconds() - start == seconds) {
			if(flag) {
				start = 0;
				flag = false;
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * The current time in milliseconds
	 * @return Double with the milliseconds.
	 */
	public double tickCountInMiliseconds() {
		double currentTimeSeconds = System.currentTimeMillis();
		return currentTimeSeconds - startMsec;
		
	}
	
	/**
 	 * The current time in seconds
	 * @return Double with the seconds.
	 */
	synchronized static int tickCountInSeconds() {
	      long currentTimeSeconds = System.currentTimeMillis() / 1000L;
	      if (_firstCallTimeSeconds == 0) {
	         _firstCallTimeSeconds = currentTimeSeconds;
	      }

	     return (int)currentTimeSeconds;
	   }
	
	public void restartSec() {
		start = 0;
		flag = false;
	}

}
