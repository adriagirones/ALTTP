package Modules;

import java.awt.Color;

import Enums.TypeDrops;
import Modules.ModuleEnemies;
import Textures.Rupee;
import Textures.Texture;
import Textures.Wall;

/**
 * Test background
 * @author Adria Girones Calzada
 *
 */
public class ModuleBackground extends Module {
	
	Texture background;
	//Texture casa;
	Wall wcasa;
	
	Animation idle = new Animation();
	
	Texture prueba;
	
	Texture rain;
	Animation animationrain = new Animation();
	
	ModuleBackground() {
		
	}
	
	boolean Start() {
		Color back = new Color(40, 32, 32);
		Main.App.window.f.setBackground(back);
		Main.App.player.Enable();
		Main.App.ui.Enable();
		Main.App.enemies.Enable();
		Main.App.entities.Enable();
		
		background = new Texture("fondo", 0, 0, 512, 512, "assets/backgrounds/mapa.png");
		
//		casa = new Texture("casa", 144, 176, 239, 271, "");
//		casa.terrain = true;
		wcasa = new Wall(144, 176, 96, 96);
		
		
		Main.App.entities.drops.add(new Rupee("rupee1", 250, 176, TypeDrops.RUPEE_GREEN));
		Main.App.entities.drops.add(new Rupee("rupee1", 280, 176, TypeDrops.RUPEE_RED));
		Main.App.entities.drops.add(new Rupee("rupee1", 320, 176, TypeDrops.RUPEE_BLUE));
		
//		idle.PushBack("assets/link/link.png");
//		idle.PushBack("assets/link/linkR.png");
//		idle.PushBack("assets/link/linkU.png");
//		idle.PushBack("assets/link/linkL.png");
//		idle.speed = 0.1f;
//		idle.loop = true;
		
		//INICIALITZAR CAMERA
		if(Main.size == 2)
			Main.App.window.f.scroll(-125, -315);
		
		//ENEMIES
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.SWORD_SOLDIER, ModuleEnemies.ENEMY_MOVE.SWORD_SOLDIER_2, 214, 58, 5, TypeDrops.NONE, 10);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.RAT, ModuleEnemies.ENEMY_MOVE.RAT_RANDOM, 419, 360 , 2, TypeDrops.HEART, 10);
		
		//rain = new Texture("rain",125-60,315-50,257-60,223-50,"");
		//rain.unscrollable = true;
//		animationrain.PushBack("assets/backgrounds/rain1.png", 257, 223);
//		animationrain.PushBack("assets/backgrounds/rain2.png", 256, 223);
//		animationrain.PushBack("assets/backgrounds/rain3.png", 256, 223);
//		animationrain.PushBack("assets/backgrounds/rain4.png", 256, 223);
//		animationrain.speed=0.5f;
		
		prueba = new Texture("casa", 183, 255+1, 183+18, 255+17+1, "");
		
		return true;
	}
	
	boolean CleanUp() {
		Main.App.ui.Disable();
		Main.App.enemies.Disable();
		Main.App.player.Disable();
		Main.App.entities.Disable();
		
		if(Main.App.window.f != null) {
			Main.App.window.f.resetScroll();
		}
			
		return true;
	}
	
	Main.update_status Update() {
		
		Main.App.textures.sprites.add(background);
		//casa.changeImage(idle.GetCurrentFrame());
		Main.App.textures.sprites.add(wcasa);
		//System.out.println(idle.Finished());
		
		Main.App.textures.sprites.add(prueba);
		
		//rain.changeFrame(animationrain.GetCurrentFrame(), Main.App.window.w.getWidth()/Main.size, Main.App.window.w.getHeight()/Main.size);
		//Main.App.textures.sprites.add(rain);
		
		if(Main.App.player.link.collidesWith(prueba)) {
			Main.App.fade.FadeToBlack(this, Main.App.level2, (float) 1.8);
		}
		
		if(Main.App.input.keysDown.contains('9')) {
			Main.App.fade.FadeToBlack(this, Main.App.level2, (float) 1.8);
		}
				
		return Main.update_status.UPDATE_CONTINUE;
	}

}
