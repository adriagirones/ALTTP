package Modules;

import java.util.ArrayList;

/**
 * Class that allows you to create animations from different image paths.
 * @author Adria Girones Calzada
 *
 */
public class Animation {
	
	/**
	 * List of the path for each frame for the animation.
	 */
	private ArrayList<String> frames = new ArrayList<>();
	
	/**
	 * List of the new x2 for each frame for the animation.
	 */
	private ArrayList<Integer> framesX = new ArrayList<>();
	
	/**
	 * List of the new y2 for each frame for the animation.
	 */
	private ArrayList<Integer> framesY = new ArrayList<>();
	
	/**
	 * Boolean that allows an animation to have a loop animation or not. It will be true if we want to have loop and false if we do not want it.
	 */
	public boolean loop = true;
	
	/**
	 * Speed at which we want to change the frames of animation. 1.0f is one frame for each iteration of the main loop.
	 */
	public float speed = 1.0f;
	
	/**
	 * Current animation frame.
	 */
	private float current_frame = 0.0f;
	
	/**
	 * Last animation frame.
	 */
	private int last_frame = 0;
	
	/**
	 * Number of loops made.
	 */
	private int loops = 0;
	
	/**
	 * Default Animation constructor.
	 */
	public Animation() {
		
	}
	
	/**
	 * Adds a frame to the animation.
	 * @param path of the image of the frame.
	 */
	public void PushBack(String path, int x2, int y2) {
		frames.add(path);
		framesX.add(x2);
		framesY.add(y2);
	}
	
	/**
	 * Function that returns the path of the image of the current frame of the animation.
	 * @return A string with the path of the current frame image.
	 */
	public String GetCurrentFrame() {
		last_frame = frames.size();
		current_frame += speed;
		if(current_frame >= last_frame) {
			current_frame = loop ? 0.0f : last_frame - 1;
			loops++;
		}

		return frames.get((int)current_frame);
	}
	
	/**
	 * Function that returns the number of the current frame of the animation.
	 * @return An integer from the list of frames in the animation specified at this time.
	 */
	public int GetCurrentNumberFrame() {
		last_frame = frames.size();
		current_frame += speed;
		if(current_frame >= last_frame) {
			current_frame = loop ? 0.0f : last_frame - 1;
			loops++;
		}
		return (int)current_frame;
	}
	
	/**
	 * Function that returns the new width of the current frame of the animation
	 * @return new x2
	 */
	public int newWidth(){
		return framesX.get(GetCurrentNumberFrame());
	}
	
	/**
	 * Function that returns the new width of the current frame of the animation
	 * @return new y2
	 */
	public int newHeight(){
		return framesY.get(GetCurrentNumberFrame());
	}
	
	/**
	 * Says if the animation is finished one time or not.
	 * @return Returns true if the animation is finished one time. <br>
	 * Returns false if the animation is not finished one time.
	 */
	public boolean Finished() {
		return loops > 0;
	}
	
	/**
	 * Resets the animation to the first frame and the variable loops to zero.
	 */
	public void Reset() {
		current_frame = 0.0f;
		loops = 0;
	}

}
