package Modules;

import java.awt.Color;
import java.util.ArrayList;

import Core.Sprite;
import Textures.Link;
import Textures.Terrain;

/**
 * Module that will create collision boxes to see if collisions work as they should.
 * @author Adria Girones Calzada
 *
 */
public class ModuleCollisionBoxes extends Module {
	
	/**
	 * List where all collision boxes are stored.
	 */
	public ArrayList<Sprite> colliders = new ArrayList<>();
	
	/**
	 * Flag when active, adds to the list of sprite the collision boxes so that they can see out the window.
	 */
	public boolean flagDebug = false;
	
	/**
	 * Default ModuleCollision() constructor.
	 */
	ModuleCollisionBoxes() {
		
	}
	
	boolean Init() {
		return true;
	}
	
	boolean CleanUp() {
		return true;
	}
	
	
	Main.update_status Update(){

		return Main.update_status.UPDATE_CONTINUE;
	}
	
	Main.update_status PostUpdate() {
		if(flagDebug) {
			for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
				int x1 = Main.App.textures.sprites.get(i).x1;
				int x2 = Main.App.textures.sprites.get(i).x2;
				int y1 = Main.App.textures.sprites.get(i).y1;
				int y2 = Main.App.textures.sprites.get(i).y2;
				
				Color color;
				if(Main.App.textures.sprites.get(i) instanceof Terrain) {
					color = new Color(0, 0, 255, 75);
					Sprite collider = new Sprite("collider", x1, y1, x2, y2, 0, color);
					colliders.add(collider);
				}
				else if(Main.App.textures.sprites.get(i).enemy) {
					color = new Color(255, 0, 0, 75);
					Sprite collider = new Sprite("collider", x1, y1, x2, y2, 0, color);
					colliders.add(collider);
				}
				else if(Main.App.textures.sprites.get(i).pj){
					color = new Color(0, 255, 0, 75);
					Sprite collider = new Sprite("collider", x1, y1, x2, y2, 0, color);
					colliders.add(collider);
				}
				else if(Main.App.textures.sprites.get(i).weaponCollider){
					color = new Color(0, 255, 255, 75);
					Sprite collider = new Sprite("collider", x1, y1, x2, y2, 0, color);
					colliders.add(collider);
				}
				else if(Main.App.textures.sprites.get(i).interact){
					color = new Color(255, 255, 0, 75);
					Sprite collider = new Sprite("collider", x1, y1, x2, y2, 0, color);
					colliders.add(collider);
				}
			}
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}
	
}
