package Modules;

import java.util.ArrayList;

import Textures.Grass;
import Textures.Pot;
import Textures.Treasure;

/**
 * Interface for the level maps
 * @author Adria Girones Calzada
 *
 */
public interface Level {
	
	/**
	 * Initialize the level camera
	 */
	public void InitializeCamera();
	
	/**
	 * Update the level pots in each iteration
	 * @param pots list of the level pots
	 */
	default void UpdatePots(ArrayList<Pot> pots) {
		for (int i = 0; i < pots.size(); i++) {
			pots.get(i).Update();
		}
		Main.App.textures.sprites.addAll(pots);
	}
	
	/**
	 * Update the level treasures in each iteration
	 * @param treasures list of the level treasures
	 */
	default void UpdateTreasures(ArrayList<Treasure> treasures) {
		Main.App.textures.sprites.addAll(treasures);
		for (int i = 0; i < treasures.size(); i++) {
			treasures.get(i).Update();
		}
	}
	
	/**
	 * Update the level grass in each iteration
	 * @param treasures list of the level grass
	 */
	default void UpdateGrass(ArrayList<Grass> grass) {
		Main.App.textures.sprites.addAll(grass);
		for (int i = 0; i < grass.size(); i++) {
			grass.get(i).Update();
		}
	}

}
