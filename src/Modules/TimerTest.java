package Modules;

import java.util.Timer;
import java.util.TimerTask;

public class TimerTest {

	public static void main(String[] args) {
		while(true) {
			//Ticks();
			timer();
		}
		
		

	}
	
	public static void timer(){
		Timer tim = new Timer();

		//comença el timer
		tim.schedule(new TimerTask() {
			@Override
			public void run() {
				//aqui el codi que vols executar
				System.out.println("A");
			}
		//aqui (en ms), el retard de la primera vegada i cada quants ms es repetra despres de la primera	
		}, 2 * 1000, 5000);
		
		//cancela el timer, deixa de comptar i s'elimina el fil
		//tim.cancel();
	}
	
	public static void Ticks() {
		System.out.println(tickCountInSeconds());
	}
	
	private static long _firstCallTimeSeconds = 0;
	
	 private synchronized static int tickCountInSeconds() {
	      long currentTimeSeconds = System.currentTimeMillis() / 1000L;
	      if (_firstCallTimeSeconds == 0) {
	         _firstCallTimeSeconds = currentTimeSeconds;
	      }

	      return (int)(currentTimeSeconds - _firstCallTimeSeconds);
	   }

}
