package Modules;

import Textures.Text;
import Textures.Texture;

/**
 * Module where all textures related to the User Interface are created.
 * @author Adria Girones Calzada
 *
 */
public class ModuleUI extends Module {
	
	Texture hud;
	Texture heart01;
	Texture heart02;
	Texture heart03;
	Texture heart04;
	Texture heart05;
	Texture heart06;
	Texture key;
	
	Texture debugMode;
	
	Text rupees;
	Text bombs;
	Text t;
	
	/**
	 * Default ModuleUI() constructor.
	 */
	ModuleUI() {
		
	}
	
	boolean Start() {
		hud = new Texture("hud", 20, 18, 240, 63, "assets/ui/hud.png");
		hud.unscrollable = true;
		
		heart01 = new Texture("empty heart", 161, 24+3, 167, 30+3, "assets/ui/heart.png");
		heart01.unscrollable = true;
		
		heart02 = new Texture("empty heart", 169, 24+3, 175, 30+3, "assets/ui/heart.png");
		heart02.unscrollable = true;
		
		heart03 = new Texture("empty heart", 177, 24+3, 183, 30+3, "assets/ui/heart.png");
		heart03.unscrollable = true;
		
		heart04 = new Texture("empty heart", 185, 24+3, 191, 30+3, "assets/ui/heart.png");
		heart04.unscrollable = true;
		
		heart05 = new Texture("empty heart", 193, 24+3, 199, 30+3, "assets/ui/heart.png");
		heart05.unscrollable = true;
		
		heart06 = new Texture("empty heart", 201, 24+3, 207, 30+3, "assets/ui/heart.png");
		heart06.unscrollable = true;
		
		key = new Texture("key", hud.x1/Main.size + 115 + 10, hud.y1/Main.size, hud.x1/Main.size + 115 + 10 + 8, hud.y1/Main.size + 8, "assets/ui/key.png");
		key.unscrollable = true;
		
		rupees = new Text("rupees", 72, 38+2, String.valueOf(Main.App.player.numRupees));
		rupees.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 18);
		rupees.unscrollable = true;
		
		bombs = new Text("rupees", 100, 38+2, String.valueOf(Main.App.player.numBombs));
		bombs.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 18);
		bombs.unscrollable = true;
		
		t = new Text("texto", 10, 176, "Hello");
		t.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 16);
		t.unscrollable = true;
		
		debugMode = new Texture("debug",0, 180, 70, 180+65, "assets/debugmode.png");
		debugMode.unscrollable = true;
		
		return true;
	}
	
	boolean CleanUp() {
		return true;
	}
	
	Main.update_status PostUpdate() {
		if(Main.App.input.modeDebug) {
			Main.App.textures.sprites.add(debugMode);
		}

		Main.App.textures.sprites.add(hud);
		
		if(Main.App.player.numKeys>0) {
			Main.App.textures.sprites.add(key);
		}
		
		switch(Main.App.player.maxLive) {
		case 6:
			Main.App.textures.sprites.add(heart01);
			Main.App.textures.sprites.add(heart02);
			Main.App.textures.sprites.add(heart03);
			Main.App.textures.sprites.add(heart04);
			Main.App.textures.sprites.add(heart05);
			Main.App.textures.sprites.add(heart06);
			break;
		case 5:
			Main.App.textures.sprites.add(heart01);
			Main.App.textures.sprites.add(heart02);
			Main.App.textures.sprites.add(heart03);
			Main.App.textures.sprites.add(heart04);
			Main.App.textures.sprites.add(heart05);
			break;
		case 4:
			Main.App.textures.sprites.add(heart01);
			Main.App.textures.sprites.add(heart02);
			Main.App.textures.sprites.add(heart03);
			Main.App.textures.sprites.add(heart04);
			break;
		case 3:
			Main.App.textures.sprites.add(heart01);
			Main.App.textures.sprites.add(heart02);
			Main.App.textures.sprites.add(heart03);
			break;
		case 2:
			Main.App.textures.sprites.add(heart01);
			Main.App.textures.sprites.add(heart02);
			break;
		case 1:
			Main.App.textures.sprites.add(heart01);
			break;
			
		}
		
		switch(Main.App.player.live) {
		case 6:
			heart01.changeImage("assets/ui/heart.png");
			heart02.changeImage("assets/ui/heart.png");
			heart03.changeImage("assets/ui/heart.png");
			heart04.changeImage("assets/ui/heart.png");
			heart05.changeImage("assets/ui/heart.png");
			heart06.changeImage("assets/ui/heart.png");
			break;
		case 5:
			heart01.changeImage("assets/ui/heart.png");
			heart02.changeImage("assets/ui/heart.png");
			heart03.changeImage("assets/ui/heart.png");
			heart04.changeImage("assets/ui/heart.png");
			heart05.changeImage("assets/ui/heart.png");
			heart06.changeImage("assets/ui/emptyheart.png");
			break;
		case 4:
			heart01.changeImage("assets/ui/heart.png");
			heart02.changeImage("assets/ui/heart.png");
			heart03.changeImage("assets/ui/heart.png");
			heart04.changeImage("assets/ui/heart.png");
			heart05.changeImage("assets/ui/emptyheart.png");
			heart06.changeImage("assets/ui/emptyheart.png");
			break;
		case 3:
			heart01.changeImage("assets/ui/heart.png");
			heart02.changeImage("assets/ui/heart.png");
			heart03.changeImage("assets/ui/heart.png");
			heart04.changeImage("assets/ui/emptyheart.png");
			heart05.changeImage("assets/ui/emptyheart.png");
			heart06.changeImage("assets/ui/emptyheart.png");
			break;
		case 2:
			heart01.changeImage("assets/ui/heart.png");
			heart02.changeImage("assets/ui/heart.png");
			heart03.changeImage("assets/ui/emptyheart.png");
			heart04.changeImage("assets/ui/emptyheart.png");
			heart05.changeImage("assets/ui/emptyheart.png");
			heart06.changeImage("assets/ui/emptyheart.png");
			break;
		case 1:
			heart01.changeImage("assets/ui/heart.png");
			heart02.changeImage("assets/ui/emptyheart.png");
			heart03.changeImage("assets/ui/emptyheart.png");
			heart04.changeImage("assets/ui/emptyheart.png");
			heart05.changeImage("assets/ui/emptyheart.png");
			heart06.changeImage("assets/ui/emptyheart.png");
			break;
		case 0:
			heart01.changeImage("assets/ui/emptyheart.png");
			heart02.changeImage("assets/ui/emptyheart.png");
			heart03.changeImage("assets/ui/emptyheart.png");
			heart04.changeImage("assets/ui/emptyheart.png");
			heart05.changeImage("assets/ui/emptyheart.png");
			heart06.changeImage("assets/ui/emptyheart.png");
			break;
		}
		
		rupees.path = String.valueOf(Main.App.player.numRupees);
		Main.App.textures.sprites.add(rupees);
		bombs.path = String.valueOf(Main.App.player.numBombs);
		Main.App.textures.sprites.add(bombs);
			
		return Main.update_status.UPDATE_CONTINUE;
	}

}
