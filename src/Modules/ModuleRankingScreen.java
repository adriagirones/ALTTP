package Modules;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import Textures.Text;
import Textures.Texture;

public class ModuleRankingScreen extends Module{
	
	Texture background;
	ArrayList<String> score = new ArrayList<>();
	ArrayList<String> time = new ArrayList<>();
	ArrayList<String> nick = new ArrayList<>();
	
	ArrayList<Text> t = new ArrayList<>();
	ArrayList<Texture> rup;
	
	/**
	 * Default ModuleLevel0() constructor.
	 */
	ModuleRankingScreen() {

	}
	
	boolean Start() {
		score.clear();
		time.clear();
		nick.clear();
		t.clear();
		
		Color back = new Color(40, 32, 32);
		Main.App.window.f.setBackground(back);
		
		background = new Texture("fondo", 0, 0, 260, 256, "assets/backgrounds/scorescreen.png");
		
		File f = new File("files/save");
		Scanner sc = null;
		try {
			sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		while(sc.hasNextLine()) {
			String s = sc.nextLine();
			String[] s2 = s.split("-");
			nick.add(s2[0]);
			time.add(s2[1]);
			score.add(s2[2]);
		}
		
		if(score.size()>0) {
			orderList(time, score, nick);
		}
		

		int space = 15;
		for (int i = 0; i < score.size(); i++) {
			Text auxt, auxt2, auxt3;
			int space2 = 0;
			//NAME
			auxt = new Text("rupees", 39+space2, 63+space, nick.get(i));
			auxt.textColor = 0xe6e6e6;
			auxt.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 16);
			space2 += 60;
			
			//SEC
			int n = Integer.valueOf(time.get(i));
			int min = n/60;
			int sec = n%60;
			
			String m1 = min >= 10 ? String.valueOf(min) : "0"+String.valueOf(min); 
			String m2 = sec >= 10 ? String.valueOf(sec) : "0"+String.valueOf(sec); 
			auxt2 = new Text("rupees", 39+space2, 63+space, m1+" . "+m2);
			auxt2.textColor = 0xe6e6e6;
			auxt2.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 16);
			space2 += 60;
			
			//SCORE
			int sco = Integer.valueOf(score.get(i));
			String ssco = sco >= 10 ? (sco >= 100 ? String.valueOf(sco) : "0"+String.valueOf(sco)) : "00"+String.valueOf(sco); 
			auxt3 = new Text("rupees", 39+space2, 63+space, ssco);
			auxt3.textColor = 0xe6e6e6;
			auxt3.font = Main.App.window.w.customFont("assets/font/alttpfont.ttf", 16);
			
			t.add(auxt);
			t.add(auxt2);
			t.add(auxt3);
			space += 15;
		}
		
		return true;
	}
	




	private void orderList(ArrayList<String> time2, ArrayList<String> score2, ArrayList<String> nick2) {
		System.out.println(time2);
		System.out.println(score2);
		System.out.println(nick2);
		ArrayList<String> AUXscore = new ArrayList<>();
		ArrayList<String> AUXtime = new ArrayList<>();
		ArrayList<String> AUXnick = new ArrayList<>();
		
		ArrayList<Integer> VALtime = new ArrayList<>();
		ArrayList<Integer> VALscore = new ArrayList<>();
		
		for (int i = 0; i < score2.size(); i++) {
			VALtime.add(Integer.valueOf(time2.get(i)));
			VALscore.add(Integer.valueOf(score2.get(i)));
		}
		
		boolean flag = false;
		while(AUXtime.size() <= 8 && !flag) {
			//System.out.println(Collections.min(VALtime));
			int index = VALtime.indexOf(Collections.min(VALtime));
			AUXscore.add(score2.get(index));
			//System.out.println(AUXscore);
			AUXtime.add(time2.get(index));
			AUXnick.add(nick2.get(index));
			time2.remove(index);
			score2.remove(index);
			nick2.remove(index);
			VALtime.remove(index);
			VALscore.remove(index);
			if (score2.size()==0) {
				flag = true;
			}
		}
		
		time2.clear();
		score2.clear();
		nick2.clear();
		
		time2.addAll(AUXtime);
		score2.addAll(AUXscore);
		nick2.addAll(AUXnick);
	}

	boolean CleanUp() {
		nick.clear();
		time.clear();
		score.clear();
		if(Main.App.window.f != null) {
			Main.App.window.f.resetScroll();
		}
			
		return true;
	}
	
	Main.update_status Update() {
		Main.App.textures.sprites.add(background);
		
		Main.App.textures.sprites.addAll(t);
	
			
		if(Main.App.input.keysDown.contains(' ')) {
			Main.App.window.w.playSFX("assets/sfx/menu close.wav");
			Main.App.fade.FadeToBlack(this, Main.App.menu, (float) 1.8);
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}


}