package Modules;

public class Main {
	
	/**
	 * Sprite size factor. Default must be 2.
	 */
	public static int size = 2;
	
	/**
	 * Organizes by creating a structure for others to perform their actions.
	 */
	public static Application App = null;
	
	/**
	 * Enumeration of the game update.
	 */
	enum update_status {
		UPDATE_CONTINUE,
		UPDATE_STOP,
		UPDATE_ERROR
	};
	
	/**
	 * Enumeration of the main states.
	 */
	public enum main_states {
		MAIN_CREATION,
		MAIN_START,
		MAIN_UPDATE,
		MAIN_FINISH,
		MAIN_EXIT
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		main_states state = main_states.MAIN_CREATION;

		while(state != main_states.MAIN_EXIT) {
			
			switch (state) {
				case MAIN_CREATION:
				{
					System.out.println("Application Creation --------");
					App = new Application();
					state = main_states.MAIN_START;
				}
				break;
				
				case MAIN_START:
				{
					System.out.println("Application Init --------------");
					if(App.Init() == false) {
						System.out.println("Application Init exits with error -----");
						state = main_states.MAIN_EXIT;
					}
					else {
						System.out.println("Application Update --------------");
						state = main_states.MAIN_UPDATE;
					}
				}
				break;
				
				case MAIN_UPDATE:
					update_status update_return = App.Update();

					if (update_return == update_status.UPDATE_ERROR) {
						System.out.println("Application Update exits with error -----");
						state = main_states.MAIN_EXIT;
					} 
					else if (update_return == update_status.UPDATE_STOP) {
						state = main_states.MAIN_FINISH;
					}
					break;
					
				case MAIN_FINISH:
				{
					System.out.println("Application CleanUp --------------");
					if(App.CleanUp() == false)
					{
						System.out.println("Application CleanUp exits with error -----");
					}

					state = main_states.MAIN_EXIT;

				} break;
						
			}
			
			Thread.sleep(33);
		}
		
	}

}
