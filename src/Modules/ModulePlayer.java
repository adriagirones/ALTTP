package Modules;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import Enemies.Enemy;
import Enums.Direction;
import Modules.Main.main_states;
import Textures.Bomb;
import Textures.Link;
import Textures.Terrain;
import Textures.Texture;

/**
 * Player module where the character is.
 * @author Adria Girones Calzada
 *
 */
public class ModulePlayer extends Module {
	
	public int numRupees = 0;
	public int numBombs = 5;
	public int numKeys = 0;
	
	Time t = new Time();
	int timerecord;
	
	int speed = 3*Main.size;
	
	public Link link;	
	
	boolean attack = false;

	boolean activeInteraction = false;
	
	public int live = 3; //184 276
	public int maxLive = 3;
	
	boolean flagMove = true;
	boolean flagAttacking = true;
	boolean dead = false;
	boolean invencible = false;
	boolean changeDeadSize = true;
	
	int knockback = 3*Main.size;
	
	Direction returnLink = Direction.NONE;
	public Direction linkDirection = Direction.DOWN;
	
	Time timerBackward = new Time();
	
	public ArrayList<Bomb> activeBombs = new ArrayList<>();
	public ArrayList<Texture> bombDamage = new ArrayList<>();
	
	/**
	 * Default ModulePlayer() constructor.
	 */
	ModulePlayer() {
		
	}
	
	boolean Start() {
		flagMove=true;
		link = new Link("link",0,0,16,24,"assets/link/link.png");
		link.changeInitialPosition();
		t.startSeconds();
		
		return true;
	}
	
	boolean CleanUp() {
//		if(Main.App.textures.sprites.contains(link)) {
//			int indexLink = Main.App.textures.sprites.indexOf(link);
//			Main.App.textures.sprites.remove(indexLink);
//		}
		return true;
	}
	
	
	Main.update_status Update() {
		
		timerecord = (int) (t.tickCountInSeconds()-t.start);
		System.out.println(timerecord);
	
		Main.App.textures.sprites.addAll(activeBombs);
		for (int i = 0; i < activeBombs.size(); i++) {
			activeBombs.get(i).Update();
		}
		Main.App.textures.sprites.addAll(bombDamage);
		
		link.changeFrame(link.animation_link.GetCurrentFrame(), link.animation_link.newWidth(), link.animation_link.newHeight());
		Main.App.textures.sprites.add(link);
		
		if(invencible) {
			System.out.println("INVENCIBLE");
		}
		
		if(flagMove && flagAttacking) {
			idle();
			Main.App.input.inputPlayer(this);
		}
		
		if(attack && flagMove) {
			swordAttack();
		}
		
		if(activeInteraction) {
			interaction();
			Main.App.textures.sprites.add(link.interaction);
		}
		
		knockbackLink();
		
		if(dead) {
			flagMove = false;
			if(returnLink == Direction.NONE) {
				if(changeDeadSize) {
					link.x2 += 8;
					link.y2 -= 8;
					changeDeadSize = false;
					Main.App.window.w.playSFX("assets/sfx/link dies.wav");
				}
				link.animation_link = link.animation_death;
			}
		}	
		
		link.collisionBoxUpdate();
		return Main.update_status.UPDATE_CONTINUE;
	}

	Main.update_status PostUpdate() {
		
		for(Enemy enemy : Main.App.enemies.enemies) {
			if(link.sword.stepsOn(enemy.texture)) {
				Main.App.window.w.playSFX("assets/sfx/enemy hit.wav");
				if(enemy.live>0) {
					enemy.live--;
				}
				if(enemy.live > 0) {
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						int count = 0;
						public void run() {
							enemy.texture.y1 += 7*Main.size;
							enemy.texture.y2 += 7*Main.size;
							for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
								if((enemy.texture.collidesWith(Main.App.textures.sprites.get(i)) && Main.App.textures.sprites.get(i) instanceof Terrain)) {
									enemy.texture.y1 -= 7*Main.size;
									enemy.texture.y2 -= 7*Main.size;
								}
							}
							if(++count == 4) {
								timer.cancel();
							}
						}
					}, 0, 15);
				}
			}
			else if(link.sword.headOn(enemy.texture)) {
				if(enemy.live>0)
					enemy.live--;
				if(enemy.live > 0) {
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						int count = 0;
						public void run() {
							enemy.texture.y1 -= 7*Main.size;
							enemy.texture.y2 -= 7*Main.size;
							for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
								if((enemy.texture.collidesWith(Main.App.textures.sprites.get(i)) && Main.App.textures.sprites.get(i) instanceof Terrain)) {
									enemy.texture.y1 += 7*Main.size;
									enemy.texture.y2 += 7*Main.size;
								}
							}
							if(++count == 4) {
								timer.cancel();
							}
						}
					}, 0, 15);
				}
			}
			else if(link.sword.rightOn(enemy.texture)) {
				if(enemy.live>0)
					enemy.live--;
				if(enemy.live > 0) {
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						int count = 0;
						public void run() {
							enemy.texture.x1 += 7*Main.size;
							enemy.texture.x2 += 7*Main.size;
							for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
								if((enemy.texture.collidesWith(Main.App.textures.sprites.get(i)) && Main.App.textures.sprites.get(i) instanceof Terrain)) {
									enemy.texture.x1 -= 7*Main.size;
									enemy.texture.x2 -= 7*Main.size;
								}
							}
							if(++count == 4) {
								timer.cancel();
							}
						}
					}, 0, 15);
				}
			}
			else if(link.sword.leftOn(enemy.texture)) {
				if(enemy.live>0)
					enemy.live--;
				if(enemy.live > 0) {
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						int count = 0;
						public void run() {
							enemy.texture.x1 -= 7*Main.size;
							enemy.texture.x2 -= 7*Main.size;
							for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
								if((enemy.texture.collidesWith(Main.App.textures.sprites.get(i)) && Main.App.textures.sprites.get(i) instanceof Terrain)) {
									enemy.texture.x1 += 7*Main.size;
									enemy.texture.x2 += 7*Main.size;
								}
							}
							if(++count == 4) {
								timer.cancel();
							}
						}
					}, 0, 15);
				}
			}
			
			for (int i = 0; i < bombDamage.size(); i++) {
				if(bombDamage.get(i).collidesWith(enemy.texture)) {
					if(enemy.live>0)
						enemy.live--;
					if(enemy.live > 0) {
						Timer timer = new Timer();
						timer.schedule(new TimerTask() {
							int count = 0;
							public void run() {
								enemy.texture.y1 -= 7*Main.size;
								enemy.texture.y2 -= 7*Main.size;
								if(++count == 4) {
									timer.cancel();
								}
							}
						}, 0, 15);
					}
				}
			}
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Function where you put the animation in idle to the character if it does not move.
	 */
	private void idle() {
		//LINK IDLE ANIMATIONS
		if(!Main.App.input.pressedKeys.contains('d') && !Main.App.input.pressedKeys.contains('a') && !Main.App.input.pressedKeys.contains('s') && !Main.App.input.pressedKeys.contains('w')) {
			switch(linkDirection) {
			case LEFT:
				if(invencible) 
					link.animation_link = link.invencibleIdleLeft;
				else 
					link.animation_link = link.idleLeft;
				break;
			case RIGHT:
				if(invencible)
					link.animation_link = link.invencibleIdleRight;
				else
					link.animation_link = link.idleRight;
				break;
			case UP:
				if(invencible)
					link.animation_link = link.invencibleIdleUp;
				else 
					link.animation_link = link.idleUp;
				break;
			case DOWN:
				if(invencible)
					link.animation_link = link.invencibleIdleDown;
				else
					link.animation_link = link.idleDown;
				break;
			default:
				break;
			}
			
		}
		
	}
	
	private void swordAttack() {
		
		link.positionSword(linkDirection);
		
		link.sword.changeFrame(link.animationSword.GetCurrentFrame(), link.animationSword.newWidth(), link.animationSword.newHeight());
		Main.App.textures.sprites.add(link.sword);
		
		if(link.animationSword.Finished()) {
			flagAttacking = true;
			attack = false;
			activeInteraction = false;
			link.sword.x1=0;
			link.sword.x2=0;
			link.sword.y1=0;
			link.sword.y2=0;
		}
		
	}
	
	/**
	 * Move the character to the left.
	 */
	public void moveLeft() {
		linkDirection = Direction.LEFT;
		link.x1-=speed;
		link.x2-=speed;
		Main.App.window.f.scroll(speed, 0);
		for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
			if(Main.App.textures.sprites.get(i) instanceof Terrain && link.leftOn(Main.App.textures.sprites.get(i))){
				link.x1+=speed;
				link.x2+=speed;
				Main.App.window.f.scroll(-speed, 0);
			}
		}
		
	}
	
	/**
	 * Move the character to the right
	 */
	public void moveRight() {
		linkDirection = Direction.RIGHT;
		link.x1+=speed;
		link.x2+=speed;
		Main.App.window.f.scroll(-speed, 0);
		for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
			if(Main.App.textures.sprites.get(i) instanceof Terrain && link.rightOn(Main.App.textures.sprites.get(i))) {
				link.x1-=speed;
				link.x2-=speed;
				Main.App.window.f.scroll(speed, 0);
			}
		}
	}
	
	/**
	 * Move the character up.
	 */
	public void moveUp() {
		linkDirection = Direction.UP;
		link.y1-=speed;
		link.y2-=speed;
		Main.App.window.f.scroll(0, speed);
		for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
			if(Main.App.textures.sprites.get(i) instanceof Terrain && link.headOn(Main.App.textures.sprites.get(i))) {
				link.y1+=speed;
				link.y2+=speed;
				Main.App.window.f.scroll(0, -speed);
			}
		}

	}
	
	/**
	 * Move the character down.
	 */
	public void moveDown() {
		linkDirection = Direction.DOWN;
		link.y1+=speed;
		link.y2+=speed;
		Main.App.window.f.scroll(0, -speed);
		for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
			if(Main.App.textures.sprites.get(i) instanceof Terrain && link.stepsOn(Main.App.textures.sprites.get(i))) {
				link.y1-=speed;
				link.y2-=speed;
				Main.App.window.f.scroll(0, speed);
			}
		}
	}
	
	/**
	 * An invisible sprite is created in front of the character that allows him to interact with entities.
	 */
	private void interaction() {
		switch(linkDirection) {
		case LEFT:
			link.interaction.x1 = link.x1 - 2*Main.size;
			link.interaction.x2 = link.interaction.x1 + 2*Main.size;
			link.interaction.y1 = link.y1;
			link.interaction.y2 = link.y2;
			break;
		case RIGHT:
			link.interaction.x1 = link.x2;
			link.interaction.x2 = link.interaction.x1 + 2*Main.size;
			link.interaction.y1 = link.y1;
			link.interaction.y2 = link.y2;
			break;
		case UP:
			link.interaction.x1 = link.x1;
			link.interaction.x2 = link.x2;
			link.interaction.y1 = link.y1 - 2*Main.size;
			link.interaction.y2 = link.interaction.y1 + 2*Main.size;
			break;
		case DOWN:
			link.interaction.x1 = link.x1;
			link.interaction.x2 = link.x2;
			link.interaction.y1 = link.y2;
			link.interaction.y2 = link.interaction.y1 + 2*Main.size;
			break;
		default:
			break;
		}
		
	}
	
	/**
	 * Knockback when the character is wounded.
	 */
	private void knockbackLink() {
		boolean flag = false;
		for (int i = 0; i < Main.App.textures.sprites.size(); i++) {
			if(Main.App.textures.sprites.get(i)  instanceof Terrain && link.collidesWith(Main.App.textures.sprites.get(i))) {
				flag = true;
				returnLink = Direction.NONE;
			}
		}	
		if(!flag) {
			switch(returnLink) { 
			case DOWN:
				link.y1+=knockback;
				link.y2+=knockback;
				Main.App.window.f.scroll(0, -knockback);
				break;
			case UP:
				link.y1-=knockback;
				link.y2-=knockback;
				Main.App.window.f.scroll(0, knockback);
				break;
			case RIGHT:
				link.x1-=knockback;
				link.x2-=knockback;
				Main.App.window.f.scroll(knockback, 0);
				break;
			case LEFT:
				link.x1+=knockback;
				link.x2+=knockback;
				Main.App.window.f.scroll(-knockback, 0);
				break;
			default:
				break;
			}
		}
	}
	
	/**
	 * The character loses a life.
	 */
	void lessLife() {
		if(live > 0 && !invencible) {
			live--;
			if(live == 1) {
				Main.App.window.w.playSFX("assets/sfx/low hp.wav");
			}
		}
		if(live == 0) {
			dead = true;
		}
	}

	public void newBomb() {
		if(numBombs > 0) {
			numBombs--;
			switch(linkDirection) {
			case LEFT:
				activeBombs.add(new Bomb(link.x1 - 15*Main.size, link.y1+5*Main.size));
				break;
			case RIGHT:
				activeBombs.add(new Bomb(link.x1 + 20*Main.size, link.y1+5*Main.size));
				break;
			case UP:
				activeBombs.add(new Bomb(link.x1 + 1*Main.size, link.y1-15*Main.size));
				break;
			case DOWN:
				activeBombs.add(new Bomb(link.x1 + 1*Main.size, link.y2+1*Main.size));
				break;
			default:
				break;
			}
		}
	}
	
	public void moreContainers() {
		if(maxLive<6) {
			maxLive++;
		}
	}

	public void moreLive() {
		if(live<maxLive) {
			live++;
		}	
	}
	
	
	public void hitPlayer(Direction returndirection, Animation animationknockback, Animation idle) {
		Main.App.window.w.playSFX("assets/sfx/link hurt.wav");
		Timer tim = new Timer();
		flagMove = false;
		lessLife();
		invencible = true;
		returnLink = returndirection;
		link.animation_link = animationknockback; 
		tim.schedule(new TimerTask() {
			public void run() {
				returnLink = Direction.NONE;
				flagMove = true;
				link.animation_link = idle;
			}
		}, 350);
		tim.schedule(new TimerTask() {
			public void run() {
				invencible = false;
			}
		}, 3000);
		
	}

	
}