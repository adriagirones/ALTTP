package Modules;

import java.awt.Color;
import java.util.ArrayList;

import Enums.TypeDrops;
import Textures.Grass;
import Textures.Texture;
import Textures.Wall;

/**
 * Module of the first level.
 * @author Adria Girones Calzada
 *
 */
public class ModuleLevel0 extends Module implements Level  {
	
	Texture background;
	Texture trees;
	Texture birdstatue;
	Animation birdstatueAnimation = new Animation();
	Texture forrest;
	Texture rain;
	Animation animationrain = new Animation();
	
	public ArrayList<Wall> walls = new ArrayList<>();
	public ArrayList<Grass> grass = new ArrayList<>();
	
	Texture tolevel1;
	public boolean fromlevel1=false;
	
	/**
	 * Default ModuleLevel0() constructor.
	 */
	ModuleLevel0() {

	}
	
	boolean Start() {
		Main.App.window.w.playMusic("assets/music/overworld.wav");
		Color back = new Color(40, 32, 32);
		Main.App.window.f.setBackground(back);
		
		InitializeCamera();
		
		Main.App.player.Enable();
		Main.App.ui.Enable();
		Main.App.enemies.Enable();
		Main.App.entities.Enable();
		
		background = new Texture("fondo", 0, 0, 1100, 1100, "assets/backgrounds/level0.png");
		trees = new Texture("trees", 286, 482, 286+499, 482+329, "assets/backgrounds/level0trees.png");
		tolevel1 = new Texture("connect", 523, 439, 523+20, 439+16, "");
		
		birdstatue = new Texture("bird", 412, 605, 412+32, 605+55, "assets/backgrounds/bird1.png");
		birdstatueAnimation.PushBack("assets/backgrounds/bird1.png", 32, 55);
		birdstatueAnimation.PushBack("assets/backgrounds/bird2.png", 32, 55);
		birdstatueAnimation.PushBack("assets/backgrounds/bird3.png", 32, 55);
		birdstatueAnimation.speed = 0.5f;
		
		//WALLS
		walls.add(new Wall(308, 456, 104, 39));
		walls.add(new Wall(313, 476, 38, 332));
		walls.add(new Wall(328, 544, 40, 46));
		walls.add(new Wall(313, 645, 54, 44)); 
		walls.add(new Wall(317, 740, 100, 55)); 
		walls.add(new Wall(393, 783, 343, 80)); 
		walls.add(new Wall(667, 739, 112, 58)); 
		walls.add(new Wall(740, 480, 55, 275)); 
		walls.add(new Wall(717, 545, 66, 45)); 
		walls.add(new Wall(717, 642, 68, 47)); 
		
		walls.add(new Wall(677, 456, 105, 40)); 
		walls.add(new Wall(545, 423, 102, 32)); 
		walls.add(new Wall(430, 426, 89, 31)); 
		walls.add(new Wall(513, 426, 39, 13)); 
		
		walls.add(new Wall(422, 411, 12, 56)); 
		walls.add(new Wall(413, 415, 14, 58)); 
		walls.add(new Wall(406, 424, 13, 56)); 
		walls.add(new Wall(398, 425, 12, 64)); 
		
		walls.add(new Wall(645, 423, 12, 44)); 
		walls.add(new Wall(654, 429, 12, 47)); 
		walls.add(new Wall(663, 427, 8, 55)); 
		walls.add(new Wall(672, 417, 9, 73)); 
		
		walls.add(new Wall(621, 683, 25, 25)); 
		walls.add(new Wall(411, 512, 88, 12)); 
		walls.add(new Wall(491, 524, 8, 45)); 
		walls.add(new Wall(563, 513, 40, 9)); 
		walls.add(new Wall(563, 522, 8, 50)); 
		
		walls.add(new Wall(502, 456, 16, 18));
		walls.add(new Wall(548, 456, 16, 18)); 
		walls.add(new Wall(659, 498, 64, 16)); 
		walls.add(new Wall(415, 629, 26, 22)); 
		
		grass.add(new Grass(462, 739, TypeDrops.NONE));
		grass.add(new Grass(478, 739, TypeDrops.RUPEE_GREEN));
		grass.add(new Grass(494 , 739, TypeDrops.NONE));
		grass.add(new Grass(510, 739, TypeDrops.NONE));
		grass.add(new Grass(526, 739, TypeDrops.HEART));
		grass.add(new Grass(542, 739, TypeDrops.NONE));
		grass.add(new Grass(558, 739, TypeDrops.NONE));
		grass.add(new Grass(574, 739, TypeDrops.RUPEE_GREEN));
		
		grass.add(new Grass(411, 525, TypeDrops.NONE));
		grass.add(new Grass(427, 525, TypeDrops.NONE));
		grass.add(new Grass(443, 525, TypeDrops.NONE));
		grass.add(new Grass(459, 525, TypeDrops.NONE));
		grass.add(new Grass(475, 525, TypeDrops.RUPEE_GREEN));
		grass.add(new Grass(427, 540, TypeDrops.NONE));
		grass.add(new Grass(443, 540, TypeDrops.NONE));
		grass.add(new Grass(459, 540, TypeDrops.NONE));
		grass.add(new Grass(475, 540, TypeDrops.NONE));
		grass.add(new Grass(427, 556, TypeDrops.RUPEE_GREEN));
		grass.add(new Grass(443, 556, TypeDrops.NONE));
		grass.add(new Grass(459, 556, TypeDrops.NONE));
		grass.add(new Grass(475, 556, TypeDrops.NONE));
		
		grass.add(new Grass(587, 541, TypeDrops.RUPEE_BLUE));
		
		forrest = new Texture("", 185,224, 185+705, 224+705, "assets/backgrounds/forrestdark.png");
		rain = new Texture("", 185, 224, 185+705, 224+705, "assets/backgrounds/rain1.png");
		animationrain.PushBack("assets/backgrounds/rain1.png", 705, 705);
		animationrain.PushBack("assets/backgrounds/rain2.png", 705, 705);
		animationrain.PushBack("assets/backgrounds/rain3.png", 705, 705);
		animationrain.PushBack("assets/backgrounds/rain4.png", 705, 705);
		animationrain.speed=0.5f;
		
		return true;
	}
	
	@Override
	public void InitializeCamera() {
		if(Main.size == 2) {
			if(fromlevel1) {
				Main.App.window.f.scroll(-401*Main.size, -340*Main.size);
			}
			else {
				Main.App.window.f.scroll(-330*Main.size, -550*Main.size);
			}
		}
	}
	
	boolean CleanUp() {
		Main.App.ui.Disable();
		Main.App.enemies.Disable();
		Main.App.player.Disable();
		Main.App.entities.Disable();
		
		if(Main.App.window.f != null) {
			Main.App.window.f.resetScroll();
		}
			
		return true;
	}
	
	Main.update_status Update() {
		Main.App.textures.sprites.add(background);
		Main.App.textures.sprites.addAll(walls);
		Main.App.textures.sprites.addAll(grass);
		UpdateGrass(grass);
		
		if(Main.App.player.link.collidesWith(tolevel1) || (Main.App.input.keysDown.contains('8') && Main.App.input.modeDebug)) {
			Main.App.fade.FadeToBlack(this, Main.App.level1, (float) 1.8);
			Main.App.level1.fromlevel0=true;
			Main.App.player.flagMove=false;
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}

	Main.update_status PostUpdate() {
		birdstatue.changeImage(birdstatueAnimation.GetCurrentFrame());
		Main.App.textures.sprites.add(birdstatue);
		Main.App.textures.sprites.add(trees);
		
		rain.changeImage(animationrain.GetCurrentFrame());
		Main.App.textures.sprites.add(forrest);
		Main.App.textures.sprites.add(rain);
		
		return Main.update_status.UPDATE_CONTINUE;
	}

}
