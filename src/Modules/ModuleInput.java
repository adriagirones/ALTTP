package Modules;

import java.util.Set;

/**
 * Module where it detects the input functions.
 * @author Adria Girones Calzada
 *
 */
public class ModuleInput extends Module {
	
	/**
	 * List of all keys that have just been pressed.
	 */
	Set<Character> keysDown;
	/**
	 * Lists all the keys that are currently being pressed.
	 */
	Set<Character> pressedKeys;
	/**
	 * List all keys that have just been raised.
	 */
	Set<Character> keysUp;
	
	/**
	 * If this boolean is activated, it allows debugging options
	 */
	boolean modeDebug = false;
	
	/**
	 * If this boolean is activated, it allows free camera option. Not recommended because it spoils the scroll
	 */
	boolean cameraMode = false;

	/**
	 * Default ModuleInput() constructor.
	 */
	ModuleInput() {
		
	}
	
	boolean Init() {
		return true;
	}
	
	Main.update_status Update() {
		
		keysDown = Main.App.window.w.getKeysDown();
		pressedKeys = Main.App.window.w.getPressedKeys();
		keysUp = Main.App.window.w.getKeysUp();
		
		if(keysDown.contains('0')) {
			modeDebug = !modeDebug;
		}
		
		if(modeDebug) {
			debugFunctions();
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}

	Main.update_status PostUpdate() {
		
		if(!keysDown.isEmpty()) {
			//System.out.println("Tecles que s'acaben d'apretar "+keysDown);
		}
		
		if(!pressedKeys.isEmpty()) {
			//System.out.println("Tecles actualment apretades "+pressedKeys);
		}
		
		if(!keysUp.isEmpty()) {
			//System.out.println("Tecles que s'acaben d'aixecar "+keysUp);
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Input for the Player.
	 * @param player Which player it is.
	 */
	public void inputPlayer(ModulePlayer player) {
		//LINK WALKING
		if(pressedKeys.contains('d')) {
			player.moveRight();
			if(player.invencible) {
				player.link.animation_link = player.link.invencibleWalkRight;
			}
			else {
				player.link.animation_link = player.link.walkRight;
			}
		}
		if(pressedKeys.contains('a')) {
			player.moveLeft();
			if(player.invencible) {
				player.link.animation_link = player.link.invencibleWalkLeft;
			}
			else {
				player.link.animation_link = player.link.walkLeft;
			}
		}
		if(pressedKeys.contains('w')) {
			player.moveUp();
			if(player.invencible) {
				player.link.animation_link = player.link.invencibleWalkUp;
			}
			else {
				player.link.animation_link = player.link.walkUp;
			}
		}
		if(pressedKeys.contains('s')) {
			player.moveDown();
			if(player.invencible) {
				player.link.animation_link = player.link.invencibleWalkDown;
			}
			else {
				player.link.animation_link = player.link.walkDown;
			}
				
		}
		
		if(keysDown.contains(' ')) {
			player.attack = true;
			player.flagAttacking = false;
			player.link.attackingDown.Reset();
			player.link.attackingRight.Reset();
			player.link.attackingLeft.Reset();
			player.link.attackingUp.Reset();
			player.link.animationSword.Reset();
			Main.App.window.w.playSFX("assets/sfx/master sword.wav");
		}
		
		if(keysDown.contains('p')) {
			player.activeInteraction = true;
		}
		else if(keysUp.contains('p')) {
			player.activeInteraction = false;
			player.link.interaction.x1 = 0;
			player.link.interaction.x2 = 0;
			player.link.interaction.y1 = 0;
			player.link.interaction.y2 = 0;
		}
		
		if(keysDown.contains('o')) {
			player.newBomb();
		}
	}
	
	/**
	 * Debug functions.
	 */
	private void debugFunctions() {
		if(keysDown.contains('1')) {
			Main.App.collisionbox.flagDebug=!Main.App.collisionbox.flagDebug;
		}
		if(keysDown.contains('2')) {
			cameraMode = !cameraMode;
		}
		if(cameraMode) {
			moveCamera();
		}
		if(keysDown.contains('3')) {
			Main.App.player.invencible=!Main.App.player.invencible;
		}
		if(keysDown.contains('4')) {
			Main.App.player.moreContainers();
		}
		if(keysDown.contains('5')) {
			Main.App.player.moreLive();
		}
		if(keysDown.contains('6')) {
			Main.App.player.numRupees++;
		}
		if(keysDown.contains('7')) {
			Main.App.player.numBombs++;
		}
		
	}
	
	/**
	 * Free camera move. Not recommended.
	 */
	private void moveCamera() {
		//MOVE CAMERA
		if(pressedKeys.contains('i')) {
			Main.App.window.f.scroll(0, 10);
		}
						
		if(pressedKeys.contains('k')) {
			Main.App.window.f.scroll(0, -10);
		}
						
		if(pressedKeys.contains('j')) {
			Main.App.window.f.scroll(10, 0);
		}
				
		if(pressedKeys.contains('l')) {
			Main.App.window.f.scroll(-10, 0);
		}
	}

}
