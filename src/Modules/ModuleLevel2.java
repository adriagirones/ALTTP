package Modules;

import java.util.ArrayList;

import Textures.Pot;
import Textures.Texture;
import Textures.Treasure;
import Textures.Wall;
import Enums.TreasureReward;
import Enums.TypeDrops;

/**
 * Module of the first level2.
 * @author Adria Girones Calzada
 *
 */
public class ModuleLevel2 extends Module implements Level {
	
	Texture background3;
	Texture door1;
	Wall walldoor;
	boolean openwalldoor = false;
	Texture lockdoor;
	
	public ArrayList<Wall> walls = new ArrayList<>();
	ArrayList<Pot> pots = new ArrayList<>();
	ArrayList<Treasure> treasures = new ArrayList<>();
	
	Texture tolevel1;
	public boolean fromlevel1 = false;
	Texture toend;
	
	
	/**
	 * Default ModuleLevel2() constructor.
	 */
	ModuleLevel2() {
		
	}
	
	boolean Start() {
		Main.App.window.w.playMusic("assets/music/dark_world_dungeon.wav");
		background3 = new Texture("fondo2", 0, 0, 748, 1024, "assets/backgrounds/background3.png");
		
		InitializeCamera();
		
		Main.App.ui.Enable();
		Main.App.player.Enable();
		Main.App.enemies.Enable();
		Main.App.entities.Enable();
		
		//WALLS
		//walls.add(new Wall(160, 147, 410, 23, ""));
		walls.add(new Wall(160, 147, 190, 22, ""));
		walls.add(new Wall(375, 147, 195, 22, ""));
		
		walls.add(new Wall(160, 147, 23, 298, ""));
		walls.add(new Wall(547, 147, 23, 298, ""));
		walls.add(new Wall(375, 422, 195, 23, ""));
		walls.add(new Wall(160, 424, 195, 23, ""));
		
		walls.add(new Wall(321, 710, 23, 231, ""));
		walls.add(new Wall(129, 710, 215, 23, ""));
		walls.add(new Wall(129, 611, 23, 122, ""));
		walls.add(new Wall(129, 611, 215, 23, ""));
		walls.add(new Wall(321, 483, 23, 151, ""));
		
		walls.add(new Wall(344, 445, 12, 61, ""));
		walls.add(new Wall(375, 445, 13, 61, ""));
		
		walls.add(new Wall(388, 483, 23, 151, ""));
		walls.add(new Wall(388, 710, 23, 231, ""));
		walls.add(new Wall(388, 710, 215, 23, ""));
		walls.add(new Wall(580, 611, 23, 122, ""));
		walls.add(new Wall(389, 611, 215, 23, ""));
		
		walls.add(new Wall(321, 920, 33, 21, ""));
		walls.add(new Wall(378, 920, 32, 21, ""));
		
		treasures.add(new Treasure("treasure", 494, 656, TreasureReward.KEY));
		
		door1 = new Texture("pasillo", 355, 436, 355+20, 436+57, "assets/backgrounds/background3_door.png");
		
		pots.add(new Pot(224, 657, TypeDrops.RUPEE_GREEN));
		
		walldoor = new Wall(350, 483, 32, 24, "");
		lockdoor = new Texture("lockdoor", 350, 483, 350+32, 483+22, "assets/backgrounds/P2lockdoor.png");
		
		tolevel1 = new Texture("connect", 356, 931, 356+20, 931+31, "assets/backgrounds/P2toP1.png");
		toend = new Texture("end", 346, 129, 346+31, 129+28,"assets/backgrounds/P2toend.png");
		
		//ENEMIES
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.SWORD_SOLDIER, ModuleEnemies.ENEMY_MOVE.SWORD_SOLDIER_2, 240, 214, 4, TypeDrops.RUPEE_GREEN, 10);
		Main.App.enemies.AddEnemy(ModuleEnemies.ENEMY_TYPES.SWORD_SOLDIER, ModuleEnemies.ENEMY_MOVE.SWORD_SOLDIER_2, 465, 214 , 4, TypeDrops.HEART, 10);
		
		return true;
	}
	
	@Override
	public void InitializeCamera() {
		if(Main.size == 2) {
			Main.App.window.f.scroll(-459, -1579);
		}	
	}
	
	boolean CleanUp() {
		if(Main.App.window.f != null)
			Main.App.window.f.resetScroll();
		
		Main.App.ui.Disable();
		Main.App.enemies.Disable();
		Main.App.player.Disable();
		Main.App.entities.Disable();
		return true;
	}
	
	Main.update_status Update() {
		Main.App.textures.sprites.add(background3);	
		Main.App.textures.sprites.addAll(walls);

		
		if(Main.App.player.numKeys>0 && walldoor.stepsOn(Main.App.player.link.interaction)) {
			openwalldoor = true;
			Main.App.player.numKeys--;
		}
		
		if(!openwalldoor) {
			Main.App.textures.sprites.add(walldoor);
			Main.App.textures.sprites.add(lockdoor);
		}
		UpdatePots(pots);
		UpdateTreasures(treasures);
		
		if(Main.App.player.link.collidesWith(tolevel1)) {
			Main.App.fade.FadeToBlack(this, Main.App.level1, (float) 1.8);
			Main.App.level1.fromlevel2=true;
			Main.App.player.flagMove=false;
		}
		
		if(Main.App.player.link.collidesWith(toend) || (Main.App.input.keysDown.contains('8') && Main.App.input.modeDebug)) {
			Main.App.fade.FadeToBlack(this, Main.App.endscreen, (float) 1.8);
		}
				
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	Main.update_status PostUpdate() {
		Main.App.textures.sprites.add(door1);
		Main.App.textures.sprites.add(tolevel1);
		Main.App.textures.sprites.add(toend);
		
		return Main.update_status.UPDATE_CONTINUE;
	}

}
