package Modules;

/**
 * Application that mainly structure the game.
 * @author Adria Girones Calzada
 *
 */
public class Application {
	
	/**
	 * Total number of modules.
	 */
	private static int NUM_MODULES = 17;
	
	/**
	 * Array with all modules.
	 */
	Module[] modules = new Module[NUM_MODULES];
	
	public ModuleWindow window;
	public ModuleTextures textures;
	public ModuleInput input;
	public ModuleBackground background;
	public ModuleMenu menu;
	public ModuleRankingScreen rankscreen;
	public ModuleLevel0 level0;
	public ModuleLevel1 level1;
	public ModuleLevel2 level2;
	public ModuleEndScreen endscreen;
	public ModuleEnemies enemies;
	public ModulePlayer player;
	public ModuleEntities entities;
	public ModuleCollisionBoxes collisionbox;
	public ModuleFadeToBlack fade;
	public ModuleUI ui;
	public ModuleRender render;
	
	/**
	 * Default Application() constructor.
	 */
	Application() {
		int i = 0;
		modules[i++] = window = new ModuleWindow();
		modules[i++] = textures = new ModuleTextures();
		modules[i++] = input = new ModuleInput();
		modules[i++] = background = new ModuleBackground();
		modules[i++] = menu = new ModuleMenu();
		modules[i++] = rankscreen = new ModuleRankingScreen();
		modules[i++] = level0 = new ModuleLevel0();
		modules[i++] = level1 = new ModuleLevel1();
		modules[i++] = level2 = new ModuleLevel2();
		modules[i++] = endscreen = new ModuleEndScreen();
		modules[i++] = player = new ModulePlayer();
		modules[i++] = enemies = new ModuleEnemies();
		modules[i++] = entities = new ModuleEntities();
		modules[i++] = collisionbox = new ModuleCollisionBoxes();
		modules[i++] = ui = new ModuleUI();
		modules[i++] = fade = new ModuleFadeToBlack();
		modules[i++] = render = new ModuleRender();
	}
	
	/**
	 * Initialization function that is called only once when starting the game. The modules to be disabled at the beginning of the game must be specified.
	 * @return Returns true if there was no error.
	 */
	public boolean Init() {
		boolean ret = true;
		
		//DISABLE MODULES
		player.Disable();
		ui.Disable();
		enemies.Disable();
		entities.Disable();
		rankscreen.Disable();
		level0.Disable();
		level1.Disable();
		level2.Disable();
		endscreen.Disable();
		background.Disable();
		
		for (int i = 0; i < NUM_MODULES && ret == true; ++i) {
			ret = modules[i].Init();
		}
		
		for (int i = 0; i < NUM_MODULES && ret == true; ++i) {
			ret = modules[i].IsEnabled() ? modules[i].Start() : true;
		}
		
		return ret;
	}
	
	/**
	 * Module main function called in each iteration of the main loop.
	 * @return Returns UPDATE_CONTINUE if the game continues in the status update.<br>
	 * Returns UPDATE_STOP if the game is stopped.
	 */
	public Main.update_status Update() {
		Main.update_status ret = Main.update_status.UPDATE_CONTINUE;
		
		for (int i = 0; i < NUM_MODULES && ret == Main.update_status.UPDATE_CONTINUE; ++i) {
			ret = modules[i].IsEnabled() ? modules[i].PreUpdate() : Main.update_status.UPDATE_CONTINUE;
		}
		for (int i = 0; i < NUM_MODULES && ret == Main.update_status.UPDATE_CONTINUE; ++i) {
			ret = modules[i].IsEnabled() ? modules[i].Update() : Main.update_status.UPDATE_CONTINUE;
		}
		for (int i = 0; i < NUM_MODULES && ret == Main.update_status.UPDATE_CONTINUE; ++i) {
			ret = modules[i].IsEnabled() ? modules[i].PostUpdate() : Main.update_status.UPDATE_CONTINUE;
		}
			
		return ret;
	}
	
	/**
	 * Function called before disabling a module.
	 * @return Returns true if there was no error.
	 */
	public boolean CleanUp() {
		boolean ret = true;

		for (int i = NUM_MODULES - 1; i >= 0 && ret == true; --i)
			ret = modules[i].CleanUp();

		return ret;
	}

}
