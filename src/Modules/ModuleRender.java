package Modules;

/**
 * Module where all sprite are rendered in the window.
 * @author Adria Girones Calzada
 *
 */
public class ModuleRender extends Module {
	
	boolean printNameSprites = false;
	

	
	/**
	 * Default ModuleRender() constructor.
	 */
	ModuleRender(){
		
	}
	
	boolean Init() {
		return true;
	}
	
	boolean CleanUp() {
		return true;
	}
	
	Main.update_status PostUpdate() {
		
		if(Main.App.collisionbox.flagDebug == true) {
			Main.App.textures.sprites.addAll(Main.App.collisionbox.colliders);
		}
		
		//PRINT LIST SPRITES
		if(printNameSprites)
			System.out.println(Main.App.textures.sprites.size());	
		
		Main.App.window.f.draw(Main.App.textures.sprites);	
		
		//CLEAN THE LISTS
		Main.App.textures.sprites.clear();
		Main.App.collisionbox.colliders.clear();
		
		return Main.update_status.UPDATE_CONTINUE;
	}

}
