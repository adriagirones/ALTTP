package Modules;

import java.awt.Color;
import Textures.Texture;

/**
 * Module of the menu screen
 * @author Adria Girones Calzada
 *
 */
public class ModuleMenu extends Module{
	
	Texture background;
	Texture arrow;
	boolean selection = false;
	
	
//	Texture tolevel1;
//	public boolean fromlevel1=false;
	
	/**
	 * Default ModuleLevel0() constructor.
	 */
	ModuleMenu() {

	}
	
	boolean Start() {
		Main.App.window.w.playMusic("assets/music/title.wav");
		selection = false;
		Color back = new Color(40, 32, 32);
		Main.App.window.f.setBackground(back);
		
		background = new Texture("fondo", 0, 0, 260, 256, "assets/backgrounds/mainmenu.png");
		arrow = new Texture("fondo", 66, 203, 66+6, 203+9, "assets/backgrounds/menuarrow.png");
		
		Main.App.player.numRupees = 0;
		Main.App.player.numBombs = 5;
		Main.App.player.numKeys = 0;
		Main.App.player.live = 3;
		Main.App.player.maxLive = 3;
		Main.App.player.t.restartSec();
		
		return true;
	}
	
	boolean CleanUp() {
		
		if(Main.App.window.f != null) {
			Main.App.window.f.resetScroll();
		}
			
		return true;
	}
	
	Main.update_status Update() {
		Main.App.textures.sprites.add(background);
		Main.App.textures.sprites.add(arrow);
		if((Main.App.input.keysDown.contains('d') || Main.App.input.keysDown.contains('a')) && !selection && !Main.App.fade.IsFading()) {
			arrow.x1 = 137*Main.size;
			arrow.x2 = 137*Main.size+6*Main.size;
			selection = !selection;
			Main.App.window.w.playSFX("assets/sfx/cursor.wav");
		}
		else if((Main.App.input.keysDown.contains('d') || Main.App.input.keysDown.contains('a')) && selection && !Main.App.fade.IsFading()) {
			arrow.x1 = 66*Main.size;
			arrow.x2 = 66*Main.size+6*Main.size;
			selection = !selection;
			Main.App.window.w.playSFX("assets/sfx/cursor.wav");
		}
			
		if(Main.App.input.keysDown.contains(' ') && !selection) {
			Main.App.window.w.playSFX("assets/sfx/secret.wav");
			Main.App.fade.FadeToBlack(this, Main.App.level0, (float) 1.8);
			
		}
		else if(Main.App.input.keysDown.contains(' ') && selection) {
			Main.App.window.w.playSFX("assets/sfx/menu open.wav");
			Main.App.fade.FadeToBlack(this, Main.App.rankscreen, (float) 1.8);
		}
		
		return Main.update_status.UPDATE_CONTINUE;
	}


}
