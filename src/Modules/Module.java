package Modules;

import java.io.IOException;

/**
 * Class which all modules will inherit their functions.
 * @author Adria Girones Calzada
 *
 */
public abstract class Module {
	
	/**
	 * Boolean specifying whether the module is active or not.
	 */
	private boolean enabled = true;
	
	/**
	 * Initialization function that is called only once when starting the game.
	 * @return Returns true if there was no error.
	 * @throws IOException 
	 */
	boolean Init() {
		return true;
	}
	
	/**
	 * Function that is called when a module is enabled.
	 * @return Returns true if there was no error.
	 * @throws IOException 
	 */
	boolean Start() {
		return true;
	}
	
	/**
	 * Module main function called in each iteration of the main loop. It goes before the Update function.
	 * @return Returns UPDATE_CONTINUE if the game continues in the status update.<br>
	 * Returns UPDATE_STOP if the game is stopped.
	 */
	Main.update_status PreUpdate() {
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Module main function called in each iteration of the main loop.
	 * @return Returns UPDATE_CONTINUE if the game continues in the status update.<br>
	 * Returns UPDATE_STOP if the game is stopped.
	 */
	Main.update_status Update() {
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Module main function called in each iteration of the main loop. It goes after the Update function.
	 * @return Returns UPDATE_CONTINUE if the game continues in the status update.<br>
	 * Returns UPDATE_STOP if the game is stopped.
	 */
	Main.update_status PostUpdate() {
		return Main.update_status.UPDATE_CONTINUE;
	}
	
	/**
	 * Function called before disabling a module.
	 * @return Returns true if there was no error.
	 */
	boolean CleanUp() {
		return true;
	}
	
	/**
	 * A function that tells whether the module is enable or not.
	 * @return Returns true if the module is active.<br>
	 * Returns false if the module is not active.
	 */
	public boolean IsEnabled() {
		return enabled;
	}
	
	/**
	 * Enables a module to be called in each iteration.
	 */
	void Enable() {
		if(enabled == false) {
			enabled = true;
			Start();
		}
	}
	
	/**
	 * Disables a module so that it is not called in each iteration.
	 */
	void Disable() {
		if(enabled == true) {
			enabled = false;
			CleanUp();
		}
	}
	
}
