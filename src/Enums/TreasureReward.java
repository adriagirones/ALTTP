package Enums;

/**
 * Enumeration for the different rewards that a treasure gives
 * @author Adrià
 *
 */
public enum TreasureReward {
	NONE,
	KEY,
	BOMBS4,
	RUPEES50,
	HEARTCONTAINER
	
}
