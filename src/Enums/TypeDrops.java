package Enums;

/**
 * Enumeration for the different type of drops
 * @author Adria Girones Calzada
 *
 */
public enum TypeDrops {
	NONE,
	RUPEE_GREEN,
	RUPEE_BLUE,
	RUPEE_RED,
	BOMBS3,
	HEART

}
