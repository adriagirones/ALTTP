package Enums;

/**
 * Enumeration for the different directions
 * @author Adria Girones Calzada
 *
 */
public enum Direction {
	NONE,
	UP,
	DOWN,
	RIGHT,
	LEFT
}
