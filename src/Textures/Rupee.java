package Textures;
import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;

public class Rupee extends Drop {

	Animation rupeeanimation = new Animation();
	
	public Rupee(String name, int x1, int y1, TypeDrops type) {
		super(name, x1*Main.size, y1*Main.size, (x1*Main.size)+(8*Main.size), (y1*Main.size)+(14*Main.size), "",type);
		if(type == TypeDrops.RUPEE_GREEN) {
			rupeeanimation.PushBack("assets/entities/rupeegreen1.png", x1, y1);
			rupeeanimation.PushBack("assets/entities/rupeegreen2.png", x1, y1);
			rupeeanimation.PushBack("assets/entities/rupeegreen3.png", x1, y1);
			rupeeanimation.speed = 0.2f;
			this.path = "assets/entities/rupeegreen1.png";
		}
		else if(type == TypeDrops.RUPEE_BLUE) {
			rupeeanimation.PushBack("assets/entities/rupeeblue1.png", x1, y1);
			rupeeanimation.PushBack("assets/entities/rupeeblue2.png", x1, y1);
			rupeeanimation.PushBack("assets/entities/rupeeblue3.png", x1, y1);
			rupeeanimation.speed = 0.2f;
			this.path = "assets/entities/rupeeblue1.png";
		}
		else if(type == TypeDrops.RUPEE_RED) {
			rupeeanimation.PushBack("assets/entities/rupeered1.png", x1, y1);
			rupeeanimation.PushBack("assets/entities/rupeered2.png", x1, y1);
			rupeeanimation.PushBack("assets/entities/rupeered3.png", x1, y1);
			rupeeanimation.speed = 0.2f;
			this.path = "assets/entities/rupeered1.png";
		}
		
	}
	
	public void changePath(){
		this.changeImage(rupeeanimation.GetCurrentFrame());
	}
	
	

}
