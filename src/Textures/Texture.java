package Textures;

import Core.Sprite;
import Modules.Main;

/**
 * Default Texture class.
 */
public class Texture extends Sprite {
	
	/**
	 * Default sprite constructor
	 * 
	 * @param name
	 *            Name of the sprite, for identification purposes
	 * @param x1
	 *            x position (horizontal) of the upper left corner
	 * @param y1
	 *            y position (vertical) of the upper left corner
	 * @param x2
	 *            x position (horizontal) of the lower right corner
	 * @param y2
	 *            y position (vertical) of the lower right corner
	 * @param path
	 *            path to the image of that sprite, as a relative path
	 */
	public Texture(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1*Main.size, y1*Main.size, x2*Main.size, y2*Main.size, path);
	}
	
	/**
	 * Default sprite constructor, for sprite that are rotated from their original
	 * image
	 * @param name
	 *            Name of the sprite, for identification purposes
	 * @param x1
	 *            x position (horizontal) of the upper left corner
	 * @param y1
	 *            y position (vertical) of the upper left corner
	 * @param x2
	 *            x position (horizontal) of the lower right corner
	 * @param y2
	 *            y position (vertical) of the lower right corner
	 * @param angle
	 *            angle (in degrees) of rotation of the sprite
	 * @param path
	 *            path to the image of that sprite, as a relative path       
	 */
	public Texture(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1*Main.size, y1*Main.size, x2*Main.size, y2*Main.size, angle, path);
	}
	
	/**
	 * Change the frame of the sprite
	 * @param path new path of the image
	 * @param x2 new width
	 * @param y2 new height
	 */
	public void changeFrame(String path, int x2, int y2) {
		this.changeImage(path);
		this.x2 = x1+(x2*Main.size);
		this.y2 = y1+(y2*Main.size);
	}

	
}
