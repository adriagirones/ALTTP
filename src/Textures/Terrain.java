package Textures;

/**
 * Interface that serves to know that something is of the terrain type and therefore cannot be crossed.
 * @author Adria Girones Calzada
 *
 */
public interface Terrain {

}
