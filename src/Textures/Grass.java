package Textures;

import java.util.Timer;
import java.util.TimerTask;

import Core.Sprite;
import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;

public class Grass extends Sprite implements Terrain {
	
	TypeDrops drop = TypeDrops.NONE;
	Texture destroyed;
	Animation animationDestroyed = new Animation();
	boolean flagDestroyed=false;
	
	public Grass(int x1, int y1, TypeDrops drop) {
		super("pot", x1*Main.size, y1*Main.size, x1*Main.size+(16*Main.size), y1*Main.size+(16*Main.size), "assets/entities/grass.png");
		this.drop = drop;
		this.terrain=true;
		
		destroyed = new Texture("grassdestroyed", x1-8, y1-17, x1-8+29, y1-17+43, "assets/entities/destroyedgrass1.png");
		
		animationDestroyed.PushBack("assets/entities/destroyedgrass1.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass2.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass3.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass4.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass5.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass6.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass7.png", 29, 43);
		animationDestroyed.PushBack("assets/entities/destroyedgrass8.png", 29, 43);
		animationDestroyed.speed=0.9f;
		animationDestroyed.loop=true;
		
	}
	
	public void Update() {
		if(this.collidesWith(Main.App.player.link.sword)) {
			flagDestroyed=true;
			this.path="";
			Timer timer = new Timer();
			Main.App.textures.sprites.add(destroyed);
			timer.schedule(new TimerTask() {
				int count = 0;
				public void run() {
					Main.App.entities.addDrop(x1, y1, drop, 10);
					if(++count == 1) {
						timer.cancel();
					}
				}
			}, 350, 150);
			Main.App.window.w.playSFX("assets/sfx/grass destroyed.wav");
			this.solid=false;
		}
		
		if(flagDestroyed && !animationDestroyed.Finished()) {
			destroyed.changeImage(animationDestroyed.GetCurrentFrame());
			Main.App.textures.sprites.add(destroyed);
		}
	}

}
