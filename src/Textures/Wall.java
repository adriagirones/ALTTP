package Textures;
import Core.Sprite;
import Modules.Main;

public class Wall extends Sprite implements Terrain {
	
	/**
	 * Default sprite constructor
	 * 
	 * @param name
	 *            Name of the sprite, for identification purposes
	 * @param x1
	 *            x position (horizontal) of the upper left corner
	 * @param y1
	 *            y position (vertical) of the upper left corner
	 * @param x2
	 *            x position (horizontal) of the lower right corner
	 * @param y2
	 *            y position (vertical) of the lower right corner
	 * @param path
	 *            path to the image of that sprite, as a relative path
	 */
	public Wall(int x1, int y1, int x2, int y2) {
		super("", x1*Main.size, y1*Main.size, x1*Main.size+ x2*Main.size, y1*Main.size+y2*Main.size, "");
		this.terrain=true;
		this.MARGIN=0;
		this.DEPTH=5;
	}
	
	public Wall(int x1, int y1, int x2, int y2, String path) {
		super("", x1*Main.size, y1*Main.size, x1*Main.size+ x2*Main.size, y1*Main.size+y2*Main.size, path);
		this.terrain=true;
		this.MARGIN=0;
	}
	
	public boolean destroyed = false;
	public boolean autoMoveActive = false;

}
