package Textures;

import java.util.Timer;
import java.util.TimerTask;

import Core.Sprite;
import Modules.Main;
import Enums.TreasureReward;

/**
 * Class for treasures that open and give rewards.
 * @author Adria Girones Calzada
 *
 */
public class Treasure extends Sprite implements Terrain {
	
	boolean flag = false;
	Texture key;
	Texture bombs4;
	Texture rupees50;
	Texture heartcontainer;
	TreasureReward reward = TreasureReward.NONE;
	boolean open = false;

	public Treasure(String name, int x1, int y1, TreasureReward reward) {
		super(name, x1*Main.size, y1*Main.size, (x1*Main.size)+(16*Main.size), (y1*Main.size)+(16*Main.size), "assets/entities/treasure.png");
		this.DEPTH=5;
		this.reward=reward;
		key = new Texture("key", (this.x1/Main.size) + 4, (this.y1/Main.size)-7,  (this.x1/Main.size) + 4 + 8, (this.y1/Main.size)-7 + 15, "assets/entities/key.png");
		bombs4 = new Texture("bombs4", (this.x1/Main.size) + 4, (this.y1/Main.size)-7,  (this.x1/Main.size) + 4 + 14, (this.y1/Main.size)-7 + 15, "assets/entities/bombs4.png");
		rupees50 = new Texture("rupees50", (this.x1/Main.size) + 4, (this.y1/Main.size)-7,  (this.x1/Main.size) + 4 + 15, (this.y1/Main.size)-7 + 15, "assets/entities/rupees50.png");
		heartcontainer = new Texture("key", (this.x1/Main.size) + 4, (this.y1/Main.size)-7,  (this.x1/Main.size) + 4 + 14, (this.y1/Main.size)-7 + 13, "assets/entities/heartcontainer.png");
	}
	
	public void Update() {
		if(this.stepsOn(Main.App.player.link.interaction) && !open) {
			System.out.println("ABRE EL COFRE----------------------------------------------");
			this.changeImage("assets/entities/treasureopen.png");
			open = true;
			
			switch(reward) {
			case KEY:
				Main.App.player.numKeys++;
				break;
			case BOMBS4:
				Main.App.player.numBombs+=4;
				break;
			case RUPEES50:
				Main.App.player.numRupees+=50;
				break;
			case HEARTCONTAINER:
				Main.App.player.moreContainers();
				Main.App.player.live = Main.App.player.maxLive;
			default:
				break;
			
			}
			
			Timer tim = new Timer();
			tim.schedule(new TimerTask() {
				public void run() {
					flag = true;
				}
			}, 0);
			tim.schedule(new TimerTask() {
				public void run() {
					flag = false;
				}
			}, 2000);
		}
		
		if(flag) {
			switch(reward) {
			case KEY:
				Main.App.textures.sprites.add(key);
				break;
			case BOMBS4:
				Main.App.textures.sprites.add(bombs4);
				break;
			case RUPEES50:
				Main.App.textures.sprites.add(rupees50);
				break;
			case HEARTCONTAINER:
				Main.App.textures.sprites.add(heartcontainer);
				break;
			default:
				break;
			}
			
		}
	}

}
