package Textures;

import Enums.Direction;
import Modules.Animation;
import Modules.Main;

public class Link extends Texture {
	
	public static Link instance = null;
	
	public Animation animation_link = new Animation();
	public Animation idleDown = new Animation();
	public Animation idleUp = new Animation();
	public Animation idleRight = new Animation();
	public Animation idleLeft = new Animation();
	public Animation walkDown = new Animation();
	public Animation walkUp = new Animation();
	public Animation walkRight = new Animation();
	public Animation walkLeft = new Animation();
	public Animation animation_death = new Animation();
	public Animation knockbackUp = new Animation();
	public Animation knockbackDown = new Animation();
	public Animation knockbackRight = new Animation();
	public Animation knockbackLeft = new Animation();
	public Animation invencibleIdleDown = new Animation();
	public Animation invencibleIdleUp = new Animation();
	public Animation invencibleIdleRight = new Animation();
	public Animation invencibleIdleLeft = new Animation();
	public Animation invencibleWalkDown = new Animation();
	public Animation invencibleWalkUp = new Animation();
	public Animation invencibleWalkRight = new Animation();
	public Animation invencibleWalkLeft = new Animation();
	public Animation attackingDown = new Animation();
	public Animation attackingRight = new Animation();
	public Animation attackingLeft = new Animation();
	public Animation attackingUp = new Animation();
	
	public Animation animationSword = new Animation();
	public Animation animationSwordDown = new Animation();
	public Animation animationSwordRight = new Animation();
	public Animation animationSwordLeft = new Animation();
	public Animation animationSwordUp = new Animation();
	
	
	public Texture sword;
	public Texture interaction;
	public Texture collisionBox;
	
	public Link(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.MARGIN=0;
		//this.pj = true;
		collisionBox = new Texture(name, x1, y1, x1+13*Main.size, y1+18*Main.size, "");
		collisionBox.pj=true;
		collisionBox.MARGIN=0;
		Animations();
	}
	
	public static Link getInstance(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		if(instance == null) {
			instance = new Link(name, x1, y1, x2, y2, path);
		}
		return instance;
	}
	
	public void Animations() {
		walkDown.PushBack("assets/link/walkD/walkD1.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD2.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD3.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD4.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD5.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD6.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD7.png",16,24);	
		walkDown.PushBack("assets/link/walkD/walkD8.png",16,24);	
		walkDown.speed = 0.2f;
		
		walkRight.PushBack("assets/link/walkR/walkR1.png",16,22);	//22
		walkRight.PushBack("assets/link/walkR/walkR2.png",16,23);	//23
		walkRight.PushBack("assets/link/walkR/walkR3.png",16,23);	//20
		walkRight.PushBack("assets/link/walkR/walkR4.png",17,23);	//20
		walkRight.PushBack("assets/link/walkR/walkR5.png",16,23);	//21
		walkRight.PushBack("assets/link/walkR/walkR6.png",16,23);	//22
		walkRight.PushBack("assets/link/walkR/walkR7.png",16,23);	//20
		walkRight.PushBack("assets/link/walkR/walkR8.png",16,23);	//21
		walkRight.speed = 0.2f;
		
		walkLeft.PushBack("assets/link/walkL/walkL1.png",16,22);	
		walkLeft.PushBack("assets/link/walkL/walkL2.png",16,23);	
		walkLeft.PushBack("assets/link/walkL/walkL3.png",16,20);	
		walkLeft.PushBack("assets/link/walkL/walkL4.png",17,20);	
		walkLeft.PushBack("assets/link/walkL/walkL5.png",16,21);	
		walkLeft.PushBack("assets/link/walkL/walkL6.png",16,22);	
		walkLeft.PushBack("assets/link/walkL/walkL7.png",16,20);	
		walkLeft.PushBack("assets/link/walkL/walkL8.png",16,21);	
		walkLeft.speed = 0.2f;
		
		walkUp.PushBack("assets/link/walkU/walkU1.png",16,21);	
		walkUp.PushBack("assets/link/walkU/walkU2.png",16,20);	
		walkUp.PushBack("assets/link/walkU/walkU3.png",16,21);	
		walkUp.PushBack("assets/link/walkU/walkU4.png",16,21);	
		walkUp.PushBack("assets/link/walkU/walkU5.png",16,21);	
		walkUp.PushBack("assets/link/walkU/walkU6.png",16,20);	
		walkUp.PushBack("assets/link/walkU/walkU7.png",16,21);	
		walkUp.PushBack("assets/link/walkU/walkU8.png",16,21);	
		walkUp.speed = 0.2f;
		
		idleDown.PushBack("assets/link/walkD/walkD1.png",16,24);	
		idleUp.PushBack("assets/link/linkU.png",16,24);	
		idleRight.PushBack("assets/link/linkR.png",17,24);	
		idleLeft.PushBack("assets/link/linkL.png",17,24);	
		
		animation_death.PushBack("assets/link/dead.png",24,16);
		
		invencibleWalkDown.PushBack("assets/link/walkD/walkD1.png",16,24);	
		invencibleWalkDown.PushBack("",16,24);	
		invencibleWalkDown.PushBack("assets/link/walkD/walkD3.png",16,24);	
		invencibleWalkDown.PushBack("",16,24);	
		invencibleWalkDown.PushBack("assets/link/walkD/walkD5.png",16,24);	
		invencibleWalkDown.PushBack("",16,24);	
		invencibleWalkDown.PushBack("assets/link/walkD/walkD7.png",16,24);	
		invencibleWalkDown.PushBack("",16,24);	
		invencibleWalkDown.speed = 0.3f;
		
		invencibleWalkRight.PushBack("assets/link/walkR/walkR1.png",16,22);	
		invencibleWalkRight.PushBack("",16,23);	
		invencibleWalkRight.PushBack("assets/link/walkR/walkR3.png",16,20);	
		invencibleWalkRight.PushBack("",17,20);	
		invencibleWalkRight.PushBack("assets/link/walkR/walkR5.png",16,21);	
		invencibleWalkRight.PushBack("",16,22);	
		invencibleWalkRight.PushBack("assets/link/walkR/walkR7.png",16,20);	
		invencibleWalkRight.PushBack("",16,21);	
		invencibleWalkRight.speed = 0.3f;
		
		invencibleWalkLeft.PushBack("assets/link/walkL/walkL1.png",16,22);	
		invencibleWalkLeft.PushBack("",16,23);	
		invencibleWalkLeft.PushBack("assets/link/walkL/walkL3.png",16,20);	
		invencibleWalkLeft.PushBack("",17,20);	
		invencibleWalkLeft.PushBack("assets/link/walkL/walkL5.png",16,21);	
		invencibleWalkLeft.PushBack("",16,22);	
		invencibleWalkLeft.PushBack("assets/link/walkL/walkL7.png",16,20);	
		invencibleWalkLeft.PushBack("",16,21);	
		invencibleWalkLeft.speed = 0.3f;
		
		invencibleWalkUp.PushBack("assets/link/walkU/walkU1.png",16,21);	
		invencibleWalkUp.PushBack("",16,20);	
		invencibleWalkUp.PushBack("assets/link/walkU/walkU3.png",16,21);	
		invencibleWalkUp.PushBack("",16,21);	
		invencibleWalkUp.PushBack("assets/link/walkU/walkU5.png",16,21);	
		invencibleWalkUp.PushBack("",16,20);	
		invencibleWalkUp.PushBack("assets/link/walkU/walkU7.png",16,21);	
		invencibleWalkUp.PushBack("",16,21);	
		invencibleWalkUp.speed = 0.3f;
		
		invencibleIdleDown.PushBack("assets/link/walkD/walkD1.png",16,24);	
		invencibleIdleDown.PushBack("",16,24);
		invencibleIdleDown.speed = 0.15f;
		
		invencibleIdleUp.PushBack("assets/link/linkU.png",16,24);	
		invencibleIdleUp.PushBack("",16,24);	
		invencibleIdleUp.speed = 0.15f;
		
		invencibleIdleRight.PushBack("assets/link/linkR.png",17,24);	
		invencibleIdleRight.PushBack("",17,24);	
		invencibleIdleRight.speed = 0.15f;
		
		invencibleIdleLeft.PushBack("assets/link/linkL.png",17,24);	
		invencibleIdleLeft.PushBack("",17,24);
		invencibleIdleLeft.speed = 0.15f;
		
		knockbackDown.PushBack("assets/link/linkfuck.png",16,24);
		knockbackDown.PushBack("assets/link/linkfuck.png",16,24);
		knockbackDown.PushBack("assets/link/linkfuck.png",16,24);
		knockbackDown.PushBack("assets/link/linkfuck.png",16,24);
		knockbackDown.speed = 0.1f;
		
		knockbackLeft.PushBack("assets/link/linkfuck3.png",16,24);
		
		knockbackRight.PushBack("assets/link/linkfuck2.png",16,24);
		
		knockbackUp.PushBack("assets/link/linkfuck4.png",16,24);
		
		attackingDown.PushBack("assets/link/attackD/linkattackD1.png", 16, 23); //1
		attackingDown.PushBack("assets/link/attackD/linkattackD1.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD2.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD2.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD3.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD4.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD4.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD4.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD4.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD5.png", 16, 23); //10
		attackingDown.PushBack("assets/link/attackD/linkattackD6.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD6.png", 16, 23);
		attackingDown.PushBack("assets/link/attackD/linkattackD6.png", 16, 23);
		attackingDown.speed = 0.4f;
		attackingDown.loop = false;
		
		attackingUp.PushBack("assets/link/attackU/linkattackU1.png", 16, 27); //1
		attackingUp.PushBack("assets/link/attackU/linkattackU1.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU2.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU2.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU3.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU4.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU4.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU4.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU4.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU5.png", 16, 27); //10
		attackingUp.PushBack("assets/link/attackU/linkattackU6.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU6.png", 16, 27);
		attackingUp.PushBack("assets/link/attackU/linkattackU6.png", 16, 27);
		attackingUp.speed = 0.4f;
		attackingUp.loop = false;
		
		attackingRight.PushBack("assets/link/attackR/linkattackR1.png", 20, 22); //1
		attackingRight.PushBack("assets/link/attackR/linkattackR1.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR2.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR3.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR4.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR5.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR5.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR5.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR5.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR4.png", 20, 22); //10
		attackingRight.PushBack("assets/link/attackR/linkattackR6.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR6.png", 20, 22);
		attackingRight.PushBack("assets/link/attackR/linkattackR6.png", 20, 22);
		attackingRight.speed = 0.4f;
		attackingRight.loop = false;
		
		attackingLeft.PushBack("assets/link/attackL/linkattackL1.png", 20, 22); //1
		attackingLeft.PushBack("assets/link/attackL/linkattackL1.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL2.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL3.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL4.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL5.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL5.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL5.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL5.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL4.png", 20, 22); //10
		attackingLeft.PushBack("assets/link/attackL/linkattackL6.png", 20, 22);
		attackingLeft.PushBack("assets/link/attackL/linkattackL6.png", 20, 22);
		attackingLeft.speed = 0.4f;
		attackingLeft.loop = false;
		
		
		animation_link = idleDown;
		
		//sword
		sword = new Texture("sword", 0, 0, 14, 13, "assets/gifsframes/sword1.png");
		sword.weaponCollider = true;
		
		animationSwordRight.PushBack("assets/link/attackR/swordR1.png",6,12);
		animationSwordRight.PushBack("assets/link/attackR/swordR2.png",8,13);
		animationSwordRight.PushBack("assets/link/attackR/swordR3.png",15,15);
		animationSwordRight.PushBack("assets/link/attackR/swordR4.png",13,14);
		animationSwordRight.PushBack("assets/link/attackR/swordR5.png",13,8);
		animationSwordRight.PushBack("assets/link/attackR/swordR6.png",13,6);
		animationSwordRight.PushBack("assets/link/attackR/swordR6.png",13,6);
		animationSwordRight.PushBack("assets/link/attackR/swordR6.png",13,6);
		animationSwordRight.PushBack("assets/link/attackR/swordR6.png",13,6);
		animationSwordRight.PushBack("assets/link/attackR/swordR7.png",13,8);
		animationSwordRight.PushBack("assets/link/attackR/swordR8.png",15,15);
		animationSwordRight.PushBack("assets/link/attackR/swordR9.png",14,13);
		animationSwordRight.PushBack("assets/link/attackR/swordR9.png",14,13);
		animationSwordRight.PushBack("assets/link/attackR/swordR9.png",14,13);
		animationSwordRight.speed = 0.35f;
		animationSwordRight.loop = false;
		
		animationSwordLeft.PushBack("assets/link/attackL/swordL1.png",6,12);
		animationSwordLeft.PushBack("assets/link/attackL/swordL2.png",8,13);
		animationSwordLeft.PushBack("assets/link/attackL/swordL3.png",15,15);
		animationSwordLeft.PushBack("assets/link/attackL/swordL4.png",13,14);
		animationSwordLeft.PushBack("assets/link/attackL/swordL5.png",13,8);
		animationSwordLeft.PushBack("assets/link/attackL/swordL6.png",13,6);
		animationSwordLeft.PushBack("assets/link/attackL/swordL6.png",13,6);
		animationSwordLeft.PushBack("assets/link/attackL/swordL6.png",13,6);
		animationSwordLeft.PushBack("assets/link/attackL/swordL6.png",13,6);
		animationSwordLeft.PushBack("assets/link/attackL/swordL7.png",13,8);
		animationSwordLeft.PushBack("assets/link/attackL/swordL8.png",15,15);
		animationSwordLeft.PushBack("assets/link/attackL/swordL9.png",14,13);
		animationSwordLeft.PushBack("assets/link/attackL/swordL9.png",14,13);
		animationSwordLeft.PushBack("assets/link/attackL/swordL9.png",14,13);
		animationSwordLeft.speed = 0.35f;
		animationSwordLeft.loop = false;
		
		animationSwordDown.PushBack("assets/link/attackD/swordD1.png",12,6);
		animationSwordDown.PushBack("assets/link/attackD/swordD2.png",13,8);
		animationSwordDown.PushBack("assets/link/attackD/swordD3.png",15,15);
		animationSwordDown.PushBack("assets/link/attackD/swordD4.png",14,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD5.png",8,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD6.png",6,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD6.png",6,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD6.png",6,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD6.png",6,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD7.png",8,13);
		animationSwordDown.PushBack("assets/link/attackD/swordD8.png",15,15);
		animationSwordDown.PushBack("assets/link/attackD/swordD9.png",13,14);
		animationSwordDown.PushBack("assets/link/attackD/swordD9.png",13,14);
		animationSwordDown.PushBack("assets/link/attackD/swordD9.png",13,14);
		animationSwordDown.speed = 0.35f;
		animationSwordDown.loop = false;
		
		animationSwordUp.PushBack("assets/link/attackU/swordU1.png",12,6);
		animationSwordUp.PushBack("assets/link/attackU/swordU2.png",13,8);
		animationSwordUp.PushBack("assets/link/attackU/swordU3.png",15,15);
		animationSwordUp.PushBack("assets/link/attackU/swordU4.png",14,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU5.png",8,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU6.png",6,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU6.png",6,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU6.png",6,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU6.png",6,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU7.png",8,13);
		animationSwordUp.PushBack("assets/link/attackU/swordU8.png",15,15);
		animationSwordUp.PushBack("assets/link/attackU/swordU9.png",13,14);
		animationSwordUp.PushBack("assets/link/attackU/swordU9.png",13,14);
		animationSwordUp.PushBack("assets/link/attackU/swordU9.png",13,14);
		animationSwordUp.speed = 0.35f;
		animationSwordUp.loop = false;
		
		
		//INTERACTION
		interaction = new Texture("sword", 0, 0, 0, 0, "");
		interaction.interact = true;
		interaction.MARGIN = 0;
	}
	
	public void positionSword(Direction linkDirection) {
		switch(linkDirection) {
		case RIGHT:
			animation_link = attackingRight;
			animationSword = animationSwordRight;
			switch(animationSword.GetCurrentNumberFrame()) {
			case 0:
				sword.x1 = x1 + (7 * Main.size);
				sword.y1 = y1 - (2 * Main.size);
				break;
			case 1:
				sword.x1 = x1 + (11 * Main.size);
				sword.y1 = y1 - (2 * Main.size);
				break;
			case 2:
				sword.x1 = x1 + (9 * Main.size);
				sword.y1 = y1 - (3 * Main.size);
				break;
			case 3:
				sword.x1 = x1 + (14 * Main.size);
				sword.y1 = y1 - (2 * Main.size);
				break;
			case 4:
				sword.x1 = x1 + (17 * Main.size);
				sword.y1 = y1 + (7 * Main.size);
				break;
			case 5:
				sword.x1 = x1 + (19 * Main.size);
				sword.y1 = y1 + (11 * Main.size);
				break;
			case 9:
				sword.x1 = x1 + (16 * Main.size);
				sword.y1 = y1 + (14 * Main.size);
				break;
			case 10:
				sword.x1 = x1 + (14 * Main.size);
				sword.y1 = y1 + (14 * Main.size);
				break;
			case 11:
				sword.x1 = x1 + (9 * Main.size);
				sword.y1 = y1 + (19 * Main.size);
				break;
			}
			break;
			
		case LEFT:
			animation_link = attackingLeft;
			animationSword = animationSwordLeft;
			switch(animationSwordLeft.GetCurrentNumberFrame()) {
			case 0:
				sword.x1 = x1 + (7 * Main.size);
				sword.y1 = y1 - (2 * Main.size);
				break;
			case 1:
				sword.x1 = x1 + (1 * Main.size);
				sword.y1 = y1 - (2 * Main.size);
				break;
			case 2:
				sword.x1 = x1 - (4 * Main.size);
				sword.y1 = y1 - (3 * Main.size);
				break;
			case 3:
				sword.x1 = x1 - (7 * Main.size);
				sword.y1 = y1 - (2 * Main.size);
				break;
			case 4:
				sword.x1 = x1 - (10 * Main.size);
				sword.y1 = y1 + (7 * Main.size);
				break;
			case 5:
				sword.x1 = x1 - (12 * Main.size);
				sword.y1 = y1 + (11 * Main.size);
				break;
			case 9:
				sword.x1 = x1 - (9 * Main.size);
				sword.y1 = y1 + (14 * Main.size);
				break;
			case 10:
				sword.x1 = x1 - (9 * Main.size);
				sword.y1 = y1 + (14 * Main.size);
				break;
			case 11:
				sword.x1 = x1 - (3 * Main.size);
				sword.y1 = y1 + (19 * Main.size);
				break;
			}
			break;
			
		case DOWN:
			animation_link = attackingDown;
			animationSword = animationSwordDown;
			switch(animationSwordDown.GetCurrentNumberFrame()) {
			case 0:
				sword.x1 = x1 - (5 * Main.size);
				sword.y1 = y1 + (11 * Main.size);
				break;
			case 1:
				sword.x1 = x1 - (6 * Main.size);
				sword.y1 = y1 + (13 * Main.size);
				break;
			case 2:
				sword.x1 = x1 - (7 * Main.size);
				sword.y1 = y1 + (11 * Main.size);
				break;
			case 3:
				sword.x1 = x1 - (6 * Main.size);
				sword.y1 = y1 + (18 * Main.size);
				break;
			case 4:
				sword.x1 = x1 + (5 * Main.size);
				sword.y1 = y1 + (20 * Main.size);
				break;
			case 5:
				sword.x1 = x1 + (9 * Main.size);
				sword.y1 = y1 + (23 * Main.size);
				break;
			case 9:
				sword.x1 = x1 + (11 * Main.size);
				sword.y1 = y1 + (18 * Main.size);
				break;
			case 10:
				sword.x1 = x1 + (10 * Main.size);
				sword.y1 = y1 + (16 * Main.size);
				break;
			case 11:
				sword.x1 = x1 + (16 * Main.size);
				sword.y1 = y1 + (14 * Main.size);
				break;
			}
			break;
			
		case UP:
			animation_link = attackingUp;
			animationSword = animationSwordUp;
			switch(animationSwordUp.GetCurrentNumberFrame()) {
			case 0:
				sword.x1 = x1 + (13 * Main.size);
				sword.y1 = y1 + (17 * Main.size);
				break;
			case 1:
				sword.x1 = x1 + (11 * Main.size);
				sword.y1 = y1 + (9 * Main.size);
				break;
			case 2:
				this.y1-=Main.size;
				this.y2-=Main.size;
				sword.x1 = x1 + (9 * Main.size);
				sword.y1 = y1 + (2 * Main.size);
				break;
			case 3:
				sword.x1 = x1 + (8 * Main.size);
				sword.y1 = y1 - (4 * Main.size);
				break;
			case 4:
				this.y1-=Main.size;
				this.y2-=Main.size;
				sword.x1 = x1 + (2 * Main.size);
				sword.y1 = y1 - (5 * Main.size);
				break;
			case 5:
				this.y1-=Main.size;
				this.y2-=Main.size;
				sword.x1 = x1 + (0 * Main.size);
				sword.y1 = y1 - (9 * Main.size);
				break;
			case 9:
				this.y1+=Main.size;
				this.y2+=Main.size;
				sword.x1 = x1 - (5 * Main.size);
				sword.y1 = y1 - (4 * Main.size);
				break;
			case 10:
				this.y1+=Main.size;
				this.y2+=Main.size;
				sword.x1 = x1 - (10 * Main.size);
				sword.y1 = y1 - (0 * Main.size);
				break;
			case 11:
				this.y1+=Main.size;
				this.y2+=Main.size;
				sword.x1 = x1 - (15 * Main.size);
				sword.y1 = y1 + (5 * Main.size);
				break;
			}
			break;
			
		default:
			break;
		}

	}
	
	public void collisionBoxUpdate() {
		collisionBox.x1 = x1+3;
		collisionBox.x2 = collisionBox.x1+13*Main.size;
		collisionBox.y1 = y1+2;
		collisionBox.y2 = collisionBox.y1+18*Main.size;
		Main.App.textures.sprites.add(collisionBox);
		
	}
	
	public void changeInitialPosition() {
			if(Main.App.background.IsEnabled()) {
				Main.App.player.link.x1 = 184*Main.size; x2 = (184+16)*Main.size; y1 = 276*Main.size; y2 = (276+24)*Main.size;
				Main.App.player.linkDirection = Direction.DOWN;
			}
					
			if(Main.App.level2.IsEnabled()) {
				if(Main.App.level2.fromlevel1) {
					Main.App.player.link.x1 = 358*Main.size; 
					Main.App.player.link.x2 = (358+16)*Main.size; 
					Main.App.player.link.y1 = 904*Main.size; 
					Main.App.player.link.y2 = (904+24)*Main.size;
					Main.App.player.linkDirection = Direction.UP;
				}
			}
			
			if(Main.App.level1.IsEnabled()) {
				if(Main.App.level1.fromlevel0) {
					Main.App.player.link.x1 = 764*Main.size; 
					Main.App.player.link.x2 = (764+16)*Main.size; 
					Main.App.player.link.y1 = 1879*Main.size; 
					Main.App.player.link.y2 = (1879+24)*Main.size;
					Main.App.player.linkDirection = Direction.UP;
					Main.App.level1.fromlevel0=false;
				}
				else if(Main.App.level1.fromlevel2) {
					Main.App.player.link.x1 = 547*Main.size; 
					Main.App.player.link.x2 = (547+16)*Main.size; 
					Main.App.player.link.y1 = 119*Main.size; 
					Main.App.player.link.y2 = (119+24)*Main.size;
					Main.App.player.linkDirection = Direction.DOWN;
					Main.App.level1.fromlevel2=false;
				}
			}
			
			if(Main.App.level0.IsEnabled()) {
				if(Main.App.level0.fromlevel1) {
					Main.App.player.link.x1 = 523*Main.size; 
					Main.App.player.link.x2 = (523+16)*Main.size; 
					Main.App.player.link.y1 = 457*Main.size; 
					Main.App.player.link.y2 = (457+24)*Main.size;
					Main.App.player.linkDirection = Direction.DOWN;
					Main.App.level0.fromlevel1=false;
				}
				else {
					Main.App.player.link.x1 = 452*Main.size; 
					Main.App.player.link.x2 = (452+16)*Main.size; 
					Main.App.player.link.y1 = 657*Main.size; 
					Main.App.player.link.y2 = (657+24)*Main.size;
					Main.App.player.linkDirection = Direction.UP;
				}
			}
	}
	
	

}
