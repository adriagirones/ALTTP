package Textures;

import Core.Sprite;
import Enums.TypeDrops;
import Modules.Animation;
import Modules.Main;

public class Pot extends Sprite implements Terrain {
	
	TypeDrops drop = TypeDrops.NONE;
	Animation destruyedanimation = new Animation();
	Texture destruyed;
	
	public Pot(int x1, int y1, TypeDrops drop) {
		super("pot", x1*Main.size, y1*Main.size, x1*Main.size+(12*Main.size), y1*Main.size+(12*Main.size), "assets/entities/pot.png");
		this.drop = drop;
		this.terrain=true;
	}
	
	public void Update() {
		if(this.collidesWith(Main.App.player.link.sword)) {
			this.path="";
			Main.App.entities.addDrop(x1, y1, drop, 10);
			this.delete=true;
		}
	}

}
