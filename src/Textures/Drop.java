package Textures;

import Core.Sprite;
import Enums.TypeDrops;

/**
 * Abstract class for drops
 * @author Adria Girones Calzada
 *
 */
public abstract class Drop extends Sprite {
	
	public TypeDrops type = TypeDrops.NONE;

	public Drop(String name, int x1, int y1, int x2, int y2, String path, TypeDrops type) {
		super(name, x1, y1, x2, y2, path);
		this.type=type;
	}
	
	public void changePath() {
		
	}
	

}
