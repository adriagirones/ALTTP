package Textures;
import Enums.TypeDrops;
import Modules.Main;

/**
 * Drop type, which adds the number of bombs the character has.
 * @author Adria Girones Calzada
 *
 */
public class BombDrop extends Drop {
	
	public BombDrop(String name, int x1, int y1, TypeDrops type) {
		super(name, x1*Main.size, y1*Main.size, (x1*Main.size)+(16*Main.size), (y1*Main.size)+(16*Main.size), "assets/entities/bombs3.png", type);
	}
	

}