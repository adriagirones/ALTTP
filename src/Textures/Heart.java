package Textures;

import Enums.TypeDrops;
import Modules.Main;

public class Heart extends Drop {
	
	public Heart(String name, int x1, int y1, TypeDrops type) {
		super(name, x1*Main.size, y1*Main.size, (x1*Main.size)+(8*Main.size), (y1*Main.size)+(7*Main.size), "assets/entities/heart.png", type);
	}

}
