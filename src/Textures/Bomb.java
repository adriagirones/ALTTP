package Textures;

import Core.Sprite;
import Enums.Direction;
import Modules.Animation;
import Modules.Main;

/**
 * Explosive bomb that destroys walls and does damage.
 * @author Adria Girones Calzada
 *
 */
public class Bomb extends Sprite {
	
	int cont = 0;
	Animation explosionAnimation = new Animation();
	Animation smokeAnimation = new Animation();
	Texture smoke;
	Texture damage;
	boolean damageFlag = false;
	
	public Bomb(int x1, int y1) {
		super("bomb", x1, y1, x1+(13*Main.size), y1+(14*Main.size), "assets/link/bomb1.png");
		explosionAnimation.PushBack("assets/link/bomb4.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb3.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb6.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb7.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb8.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb1.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb2.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb5.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb4.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb3.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb6.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb7.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb8.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb1.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb2.png", 13, 14);
		explosionAnimation.PushBack("assets/link/bomb5.png", 13, 14);
		explosionAnimation.loop=false;
		explosionAnimation.speed=0.4f;
		
		smoke = new Texture("damageExplosion", (x1/Main.size)-(19*Main.size), (y1/Main.size)-(20*Main.size), (x1/Main.size)-(19*Main.size)+(44*Main.size), (y1/Main.size)-(20*Main.size)+(46*Main.size), "assets/link/explosion1");
		smokeAnimation.PushBack("assets/link/explosion1.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion2.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion3.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion4.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion5.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion6.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion7.png", 44, 46);
		smokeAnimation.PushBack("assets/link/explosion8.png", 44, 46);
		smokeAnimation.loop=false;
		smokeAnimation.speed=0.2f;
		

		damage = new Texture("", (x1/Main.size)-(11*Main.size), (y1/Main.size)-(8*Main.size), (x1/Main.size)-(11*Main.size)+(27*Main.size), (y1/Main.size)-(8*Main.size)+(24*Main.size), "");
		damage.weaponCollider=true;
		
	}
	
	public void Update() {
		cont++;
		if(cont >= 80) {
			this.path = explosionAnimation.GetCurrentFrame();
			if(explosionAnimation.Finished()) {
				this.path="";
				smoke.changeImage(smokeAnimation.GetCurrentFrame());
				Main.App.textures.sprites.add(smoke);
				if(smokeAnimation.GetCurrentNumberFrame()==2 && !damageFlag) {
					Main.App.window.w.playSFX("assets/sfx/bomb explode.wav");
					Main.App.player.bombDamage.add(damage);
					damageFlag = true;
					characterCollision();
				}
				if(smokeAnimation.Finished()) {
					Main.App.player.bombDamage.remove(Main.App.player.bombDamage.indexOf(damage));
					Main.App.player.activeBombs.remove(this);
				}
			}
			
		}
		
	}
	
	void characterCollision(){
		//IF THE CHARACTER COLLIDES WITH THE BOMB WHEN EXPLODES
		if(damage.stepsOn((Main.App.player.link))) {
			Main.App.player.hitPlayer(Direction.DOWN, Main.App.player.link.knockbackDown, Main.App.player.link.idleUp);
		}
		else if(damage.headOn((Main.App.player.link))) {
			Main.App.player.hitPlayer(Direction.UP, Main.App.player.link.knockbackUp, Main.App.player.link.idleDown);
		}
		else if(damage.leftOn((Main.App.player.link))) {
			Main.App.player.hitPlayer(Direction.RIGHT, Main.App.player.link.knockbackRight, Main.App.player.link.idleLeft);
		}
		else if(damage.rightOn((Main.App.player.link))) {
			Main.App.player.hitPlayer(Direction.LEFT, Main.App.player.link.knockbackLeft, Main.App.player.link.idleRight);
		}
		else if(damage.collidesWith(Main.App.player.link)) {
			switch(Main.App.player.linkDirection) {
			case UP:
				Main.App.player.hitPlayer(Direction.UP, Main.App.player.link.knockbackUp, Main.App.player.link.idleDown);
				break;
			case DOWN:
				Main.App.player.hitPlayer(Direction.DOWN, Main.App.player.link.knockbackDown, Main.App.player.link.idleUp);
				break;
			case RIGHT:
				Main.App.player.hitPlayer(Direction.RIGHT, Main.App.player.link.knockbackRight, Main.App.player.link.idleLeft);
				break;
			case LEFT:
				Main.App.player.hitPlayer(Direction.LEFT, Main.App.player.link.knockbackLeft, Main.App.player.link.idleRight);
				break;
			default:
				break;
				
			}

		}
	}
	
	

}
