**CONTROLS**
> * A -> Move Left
> * S -> Move Down
> * D -> Move Right
> * W -> Move Up
> * SPACE -> Attack
> * O -> Bomb
> * P -> Interaction
> * 0 -> Active debug functions

